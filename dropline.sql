-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2015 at 02:35 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dropline`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `items`, `expire_date`) VALUES
(2, '[{"id":"45","size":"Medium","quantity":"5"}]', '2015-12-23 05:09:37'),
(3, '[{"id":"45","size":"Small","quantity":"2"}]', '2015-12-23 05:48:57'),
(4, '[{"id":"45","size":"Medium","quantity":"2"}]', '2015-12-23 05:53:26'),
(5, '[{"id":"45","size":"Small","quantity":"3"},{"id":"44","size":"Medium","quantity":"2"}]', '2015-12-23 06:08:57'),
(6, '[{"id":"45","size":"Small","quantity":3}]', '2015-12-23 08:46:17'),
(7, '[{"id":"44","size":"Medium","quantity":"12"}]', '2015-12-23 10:18:08'),
(9, '[{"id":"45","size":"Medium","quantity":"1"}]', '2015-12-23 10:24:31'),
(10, '[{"id":"46","size":"Small","quantity":"1"}]', '2015-12-25 08:09:10'),
(13, '[{"id":"45","size":"Medium","quantity":"5"}]', '2015-12-25 15:14:07'),
(14, '[{"id":"45","size":"Medium","quantity":"2"}]', '2015-12-25 15:25:19'),
(15, '[{"id":"44","size":"Small","quantity":"1"}]', '2015-12-25 15:35:22'),
(17, '[{"id":"44","size":"Small","quantity":"1"}]', '2015-12-26 03:50:06'),
(18, '[{"id":"45","size":"Medium","quantity":"2"}]', '2015-12-26 03:59:14'),
(20, '[{"id":"45","size":"Small","quantity":"2"}]', '2015-12-26 05:51:46'),
(24, '[{"id":"45","size":"Medium","quantity":3}]', '2015-12-26 09:59:15'),
(25, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-26 15:30:20'),
(26, '[{"id":"45","size":"Small","quantity":"2"}]', '2015-12-26 15:33:41'),
(27, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-26 15:35:35'),
(28, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-26 15:49:15'),
(29, '[{"id":"44","size":"Medium","quantity":"1"}]', '2015-12-27 12:01:39'),
(31, '[{"id":"44","size":"Small","quantity":"1"},{"id":"45","size":"Small","quantity":"1"}]', '2015-12-27 15:15:06'),
(32, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-27 15:55:38'),
(33, '[{"id":"44","size":"Small","quantity":"1"}]', '2015-12-27 15:57:00'),
(36, '[{"id":"44","size":"Small","quantity":"1"},{"id":"45","size":"Small","quantity":2}]', '2015-12-28 05:47:39'),
(38, '[{"id":"44","size":"Medium","quantity":"2"},{"id":"45","size":"Medium","quantity":2}]', '2015-12-28 06:32:28'),
(39, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-28 07:10:13'),
(40, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-28 07:14:52'),
(41, '[{"id":"44","size":"Small","quantity":"1"}]', '2015-12-28 07:18:47'),
(42, '[{"id":"44","size":"Small","quantity":"2"}]', '2015-12-28 07:38:29'),
(43, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-28 08:04:42'),
(44, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-28 09:21:00'),
(45, '[{"id":"44","size":"Medium","quantity":"2"},{"id":"45","size":"Small","quantity":"1"}]', '2015-12-28 09:29:10'),
(46, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-28 09:37:41'),
(47, '[{"id":"44","size":"Medium","quantity":"2"},{"id":"45","size":"Small","quantity":"3"}]', '2015-12-28 11:19:36'),
(48, '[{"id":"44","size":"Small","quantity":"1"},{"id":"45","size":"Small","quantity":"1"}]', '2015-12-28 15:13:16'),
(49, '[{"id":"44","size":"Medium","quantity":"1"},{"id":"45","size":"Large","quantity":"2"}]', '2015-12-29 04:17:42'),
(50, '[{"id":"44","size":"Small","quantity":"2"}]', '2015-12-29 17:29:58'),
(51, '[{"id":"45","size":"Small","quantity":"2"}]', '2015-12-29 20:08:44'),
(52, '[{"id":"44","size":"Small","quantity":"2"},{"id":"45","size":"Small","quantity":"2"}]', '2015-12-30 00:48:42'),
(53, '[{"id":"44","size":"Medium","quantity":"2"}]', '2015-12-30 01:42:58'),
(54, '[{"id":"44","size":"Small","quantity":"3"}]', '2015-12-31 02:12:39'),
(55, '[{"id":"45","size":"Small","quantity":"2"}]', '2015-12-31 02:19:20'),
(56, '[{"id":"45","size":"Small","quantity":"2"}]', '2015-12-31 02:20:57'),
(57, '[{"id":"45","size":"Small","quantity":"1"}]', '2015-12-31 02:25:35'),
(58, '[{"id":"44","size":"Medium","quantity":"1"},{"id":"45","size":"Small","quantity":"1"}]', '2015-12-31 06:27:28'),
(59, '[{"id":"45","size":"Medium","quantity":"2"}]', '2015-12-31 13:21:38'),
(60, '[{"id":"50","size":"Medium","quantity":3}]', '2015-12-31 16:55:19'),
(61, '[{"id":"50","size":"Medium","quantity":"1"},{"id":"44","size":"Small","quantity":"1"}]', '2015-12-31 17:23:00'),
(62, '[{"id":"50","size":"Medium","quantity":"1"}]', '2015-12-31 17:27:40'),
(63, '[{"id":"45","size":"Small","quantity":5}]', '2015-12-31 17:30:39'),
(64, '[{"id":"50","size":"Medium","quantity":"3"}]', '2016-01-01 04:16:12'),
(65, '[{"id":"50","size":"Medium","quantity":"2"},{"id":"44","size":"Medium","quantity":"2"}]', '2016-01-01 12:34:39'),
(66, '[{"id":"50","size":"Small","quantity":"2"}]', '2016-01-01 12:43:49'),
(67, '[{"id":"45","size":"Small","quantity":"1"}]', '2016-01-01 12:44:51'),
(68, '[{"id":"50","size":"Small","quantity":"2"}]', '2016-01-01 12:59:28'),
(69, '[{"id":"50","size":"Small","quantity":"1"}]', '2016-01-01 13:06:03'),
(70, '[{"id":"50","size":"Medium","quantity":"3"},{"id":"45","size":"Large","quantity":"3"}]', '2016-01-02 03:55:17'),
(71, '[{"id":"45","size":"Medium","quantity":"3"}]', '2016-01-02 04:00:48'),
(72, '[{"id":"50","size":"Medium","quantity":500},{"id":"44","size":"Small","quantity":5}]', '2016-01-02 07:47:05'),
(73, '[{"id":"44","size":"Small","quantity":"1"}]', '2016-01-02 07:51:21'),
(74, '[{"id":"45","size":"Medium","quantity":"1"}]', '2016-01-02 07:52:06'),
(75, '[{"id":"50","size":"Small","quantity":"1"}]', '2016-01-02 10:56:11'),
(76, '[{"id":"45","size":"Medium","quantity":2}]', '2016-01-03 05:23:45'),
(77, '[{"id":"50","size":"Small","quantity":"3"},{"id":"45","size":"Small","quantity":"2"}]', '2016-01-04 18:28:41'),
(78, '[{"id":"44","size":"Medium","quantity":"2"}]', '2016-01-04 18:37:28');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `parent`) VALUES
(15, 'Men', 0),
(16, 'V-Neck', 15),
(17, 'Women', 0),
(18, 'Tank Top', 17),
(19, 'Anime', 0),
(20, 'One Punch Man', 19),
(21, 'Attack', 19);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `sizes` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categories` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(4) NOT NULL DEFAULT '0',
  `image` text COLLATE utf8_unicode_ci NOT NULL,
  `archived` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `price`, `sizes`, `categories`, `featured`, `image`, `archived`) VALUES
(44, 'Marvel Grey', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', '250.00', 'Small,Medium,Large', '16', 1, '/dropline/images/products/327f365b24395a463af91801c578b9f9.jpg', 0),
(45, 'Retro Blue', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ', '250.00', 'Small,Medium,Large', '18', 1, '/dropline/images/products/5c2284b2be616504aac2530e877309f5.jpg', 0),
(46, 'Cosmo Black', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', '300.00', 'Small,Medium,Large,XL', '16', 0, '/dropline/images/products/15bc55da2efaedaa902f57c9c84cad71.jpeg', 1),
(50, 'Test Shirts 03', 'A sample description.', '100.00', 'Small,Medium,Large', '20', 1, '/dropline/images/products/4136e1f084477c440e0f684ab9b3d4f7.jpg,/dropline/images/products/9943027464239b23cfaf7e1c311205b5.jpg,/dropline/images/products/3eb52137a22eae707409d1fcb3eff1d2.jpg,/dropline/images/products/e01ed93e1b7b4e21fce86a9c156875b7.jpeg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `cart_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `ordertotal` decimal(10,2) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `trans_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paid` tinyint(4) NOT NULL DEFAULT '0',
  `completed_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `cart_id`, `full_name`, `email`, `phone`, `street`, `street2`, `city`, `state`, `zipcode`, `country`, `subtotal`, `ordertotal`, `description`, `trans_date`, `paid`, `completed_date`) VALUES
(30, '17', 'Mar Roxas', 'mar.roxas@gmail.com', '09095289109', 'Mahayahay', 'Purok 12', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '250.00', '250.00', '1 item purchased.', '2015-11-26 10:56:52', 1, '2015-11-28 15:15:39'),
(31, '18', 'Jejomar Binay', 'jejomar.binay@gmail.com', '09023475810', 'Bagong Silang', 'Purok 4', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-11-26 11:00:22', 1, '2015-12-03 04:07:52'),
(32, '20', 'Jedidiah Avelino', 'jed.avelino@gmail.com', '09058892904', 'Suarez', 'Zone Gemini', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-11-26 12:56:32', 1, '2015-11-26 09:58:17'),
(53, '41', 'Grace Poe', 'grace.poe@gmail.com', '09023475810', 'Luinab', 'Purok 20', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '250.00', '250.00', '1 item purchased.', '2015-11-28 14:19:47', 1, '2015-11-28 07:31:15'),
(54, '42', 'Rodrigo Duterte', 'rodrigo.duterte@gmail.com', '09075809581', 'Bagong Silang', 'Purok 9', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-11-28 14:47:21', 1, '2015-11-28 09:11:55'),
(56, '43', 'Liza Concepcion', 'liza.concepcion@gmail.com', '09058892904', 'Suarez', 'Zone Gemini', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '250.00', '250.00', '1 item purchased.', '2015-11-28 16:15:14', 0, '0000-00-00 00:00:00'),
(57, '44', 'Alma Moreno', 'alma.moreno@gmail.com', '09058892904', 'Suarez', 'Zone Gemini', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '250.00', '250.00', '1 item purchased.', '2015-11-28 16:22:32', 0, '0000-00-00 00:00:00'),
(58, '45', 'Karen Davila', 'karen.davila@gmail.com', '09075809581', 'Buru-un', 'Zone Taurus', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '1500.00', '1500.00', '3 items purchased.', '2015-11-28 16:32:04', 1, '2015-11-28 09:37:20'),
(59, '46', 'Miriam Santiago', 'miriam.santiago@gmail.com', '09075809581', 'Tubod', 'Purok 4', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '250.00', '250.00', '1 item purchased.', '2015-11-28 16:41:28', 0, '0000-00-00 00:00:00'),
(60, '47', 'Joseph Estrada', 'joseph.estrada@gmail.com', '09075809581', 'Suarez', 'Zone Gemini', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2500.00', '2500.00', '5 items purchased.', '2015-11-28 18:22:00', 0, '0000-00-00 00:00:00'),
(61, '48', 'Wes Anderson', 'wes.anderson@gmail.com', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-11-28 22:14:21', 1, '2015-12-01 02:14:57'),
(63, '49', 'Major Lazer', 'major.lazer@gmail.com', '09075809581', 'Bara-as', 'Purok 3', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '750.00', '750.00', '3 items purchased.', '2015-11-30 00:20:36', 0, '0000-00-00 00:00:00'),
(64, '50', 'Jackie Chan', 'jackie.chan@yahoo.com', '09023475810', 'Fuentes', 'Purok 12', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-11-30 03:05:51', 1, '2015-11-29 20:16:07'),
(65, '51', 'Jackie Chan', 'jackie.chan@yahoo.com', '09023475810', 'Fuentes', 'Purok 12', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-11-30 03:13:31', 1, '2015-12-01 02:18:23'),
(66, '52', 'Wes Anderson', 'wes.anderson@gmail.com', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '1000.00', '1000.00', '4 items purchased.', '2015-11-30 08:30:49', 1, '2015-12-01 02:20:28'),
(67, '53', 'Son Guko', 'son.guko@gmail.com', '09023475810', 'Canaway', 'Purok 15', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-11-30 08:49:55', 0, '0000-00-00 00:00:00'),
(68, '54', 'Wes Anderson', 'wes.anderson@gmail.com', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '750.00', '750.00', '3 items purchased.', '2015-12-01 09:13:14', 1, '2015-12-01 02:20:24'),
(69, '55', 'Wes Anderson', 'wes.anderson@gmail.com', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-12-01 09:19:31', 1, '2015-12-01 02:19:51'),
(70, '56', 'Wes Anderson', 'wes.anderson@gmail.com', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-12-01 09:21:15', 0, '0000-00-00 00:00:00'),
(71, '57', 'Wes Anderson', 'wes.anderson@gmail.com', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '250.00', '250.00', '1 item purchased.', '2015-12-01 09:25:46', 0, '0000-00-00 00:00:00'),
(72, '58', 'Stephanie Espolong', 'stephanie.espolong@gmail.com', '09158626694', 'Tibanga', 'Elena Tower', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-12-01 13:29:24', 0, '0000-00-00 00:00:00'),
(73, '59', 'Gene Manlimos', 'gene.manlimos@gmail.com', '09023475810', 'San Miguel', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-12-01 23:53:45', 1, '2015-12-01 16:54:28'),
(74, '60', 'Gene Manlimos', 'gene.manlimos@gmail.com', '09023475810', 'San Miguel', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '300.00', '300.00', '3 items purchased.', '2015-12-01 23:57:40', 1, '2015-12-01 17:21:02'),
(75, '61', 'Gene Manlimos', 'gene.manlimos@gmail.com', '09023475810', 'San Miguel', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-12-02 00:23:27', 0, '0000-00-00 00:00:00'),
(76, '62', 'Gene Manlimos', 'gene.manlimos@gmail.com', '09023475810', 'San Miguel', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '100.00', '100.00', '1 item purchased.', '2015-12-02 00:27:51', 0, '0000-00-00 00:00:00'),
(77, '63', 'Gene Manlimos', 'gene.manlimos@gmail.com', '09023475810', 'San Miguel', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '1250.00', '1250.00', '5 items purchased.', '2015-12-02 00:31:39', 0, '0000-00-00 00:00:00'),
(78, '64', 'Wes Anderson', 'wes.anderson@gmail.com', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '300.00', '300.00', '3 items purchased.', '2015-12-02 11:17:26', 0, '0000-00-00 00:00:00'),
(79, '65', 'Pope Francis', 'pope.francis@gmail.com', '09075809581', 'Tibanga', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '1000.00', '1000.00', '4 items purchased.', '2015-12-02 19:39:41', 0, '0000-00-00 00:00:00'),
(80, '66', 'Pope Francis', 'pope.francis@gmail.com', '09075809581', 'Tibanga', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '200.00', '200.00', '2 items purchased.', '2015-12-02 19:44:17', 0, '0000-00-00 00:00:00'),
(81, '67', 'Pope Francis', 'pope.francis@gmail.com', '09075809581', 'Tibanga', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '250.00', '250.00', '1 item purchased.', '2015-12-02 19:44:59', 0, '0000-00-00 00:00:00'),
(82, '68', 'Adam Levigne', 'adam.levigne@gmail.com', '09023475810', 'Tominobo', 'Purok 10', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '400.00', '400.00', '2 items purchased.', '2015-12-02 20:01:09', 0, '0000-00-00 00:00:00'),
(83, '69', 'Adam Levigne', 'adam.levigne@gmail.com', '09023475810', 'Tominobo', 'Purok 10', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '100.00', '100.00', '1 item purchased.', '2015-12-02 20:08:08', 1, '2015-12-02 13:09:00'),
(84, '70', 'Jewell Manlimos', 'info@gmail.com', '09201920321', 'iligan city', 'door 2', 'iligan city', 'Lanao del Norte', '9200', 'Philippines', '1500.00', '1500.00', '6 items purchased.', '2015-12-03 10:58:38', 0, '0000-00-00 00:00:00'),
(85, '71', 'Jewell Manlimos', 'info@gmail.com', '09201920321', 'iligan city', 'door 2', 'iligan city', 'Lanao del Norte', '9200', 'Philippines', '750.00', '750.00', '3 items purchased.', '2015-12-03 11:02:26', 0, '0000-00-00 00:00:00'),
(86, '72', 'Eden Ibit', 'edenmae@gmail.com', '12131231231', 'ddakjsdh', 'kjhsjdkfhskjdf', '131sdkjsd', 'sdsdfsdfsf', '3e29374', 'Philippines', '126250.00', '126250.00', '505 items purchased.', '2015-12-03 14:49:18', 0, '0000-00-00 00:00:00'),
(87, '73', 'Eden Ibit', 'edenmae@gmail.com', '12131231231', 'ddakjsdh', 'kjhsjdkfhskjdf', '131sdkjsd', 'sdsdfsdfsf', '3e29374', 'Philippines', '250.00', '250.00', '1 item purchased.', '2015-12-03 14:51:54', 0, '0000-00-00 00:00:00'),
(88, '74', 'Eden Ibit', 'edenmae@gmail.com', '12131231231', 'ddakjsdh', 'kjhsjdkfhskjdf', '131sdkjsd', 'sdsdfsdfsf', '3e29374', 'Philippines', '250.00', '250.00', '1 item purchased.', '2015-12-03 14:52:24', 1, '2015-12-03 07:54:03'),
(89, '75', 'mayriele', 'gen@yahoo.com', '09287789131', '212 suarez', '', 'Iligan', 'Lanao', '9200', 'Philippines', '100.00', '100.00', '1 item purchased.', '2015-12-03 17:57:20', 0, '0000-00-00 00:00:00'),
(90, '76', 'Lorena ', 'lorenakarla22@gmail.com', '0948312345', 'Tibanga', 'Mariquit', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-12-04 12:28:34', 0, '0000-00-00 00:00:00'),
(91, '77', 'Wes Anderson', 'wes.anderson@gmail.com', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '0.00', '0.00', '0 item purchased.', '2015-12-06 01:34:13', 0, '0000-00-00 00:00:00'),
(92, '78', 'Wes Anderson', 'wes.anderson@gmail.com', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '500.00', '500.00', '2 items purchased.', '2015-12-06 01:37:40', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(175) COLLATE utf8_unicode_ci NOT NULL,
  `join_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime NOT NULL,
  `permissions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `deactivated` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `password`, `phone`, `street`, `street2`, `city`, `state`, `zipcode`, `country`, `join_date`, `last_login`, `permissions`, `deactivated`) VALUES
(1, 'Jedidiah Avelino', 'jed.avelino@gmail.com', '$2y$10$qDXL.8eX7DBoD2dqwAUrnuzDx7CQwgZkyhsw1WL9Vt.53h3h3dQkC', '09279129736', 'Suarez', 'Zone Gemini', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-09-24 21:59:35', '2015-12-06 02:32:21', 'admin,editor', 0),
(2, 'Francis Avelino', 'francis@yahoo.com', '$2y$10$XAeu4JFVuydVS/60.OT/qOdQRwMlruhDaxrofWkvodwVdYjrH2/Mu', '', '', '', '', '', '', '', '2015-11-25 18:50:07', '2015-12-06 02:31:25', 'admin,editor', 0),
(7, 'Mar Roxas', 'mar.roxas@gmail.com', '$2y$10$1G3WdlfYU28/5f3RKv02aOCvdJK4WoIG0Wp9YEqdlAgMaLTxkXJ5q', '09023475810', 'Suarez', 'Zone Gemini', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 14:02:35', '2015-11-29 16:54:10', 'customer', 0),
(8, 'Jojo Binay', 'jojo.binay@gmail.com', '$2y$10$Byra2CoIVUpVH95vM36zK..t6GGXbG9K0kqEpmAbQC45IcRh/6cky', '09058892904', 'Saray', 'Purok 7', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 14:11:51', '0000-00-00 00:00:00', 'customer', 0),
(9, 'Korina Sanches', 'korina.sanchez@gmail.com', '$2y$10$N.UXIEsalg7nF3qezwaw7eWh4OUWXvToj45TVbd2S.8qquFF8pCyS', '09075809581', 'Hinaplanon', 'Purok 15', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 14:16:28', '0000-00-00 00:00:00', 'customer', 1),
(10, 'Grace Poe', 'grace.poe@gmail.com', '$2y$10$6ca/nGe/SU3Bday9JyGgU.aadZM3AKuYcqQUv4fOhhW5HLPwKIax.', '09023475810', 'Luinab', 'Purok 20', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 14:19:47', '0000-00-00 00:00:00', 'customer', 0),
(11, 'Rodrigo Duterte', 'rodrigo.duterte@gmail.com', '$2y$10$J2L3l3qVllcxLDDxm1N1AeWgAqRd.FL/DHaFioAcpkKW/Fj7fDN1q', '09075809581', 'Bagong Silang', 'Purok 9', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 14:47:21', '2015-11-28 10:14:15', 'customer', 0),
(13, 'Liza Concepcion', 'liza.concepcion@gmail.com', '$2y$10$/Pqg4rDCCTv/h5f6RbwxyO0Ah6hkfJ9ZlDjs56DyFkvmFITZghAVS', '09058892904', 'Suarez', 'Zone Gemini', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 16:15:14', '0000-00-00 00:00:00', 'customer', 0),
(14, 'Alma Moreno', 'alma.moreno@gmail.com', '$2y$10$j72ARLZKmYMPVTovVwJMjOYtQ4cI.A7IN2wHypf4aDkVTJ5pxRjrS', '09058892904', 'Suarez', 'Zone Gemini', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 16:22:32', '0000-00-00 00:00:00', 'customer', 0),
(15, 'Karen Davila', 'karen.davila@gmail.com', '$2y$10$VqwsLOLQ/k7gWPhvT2nlcu1NlzQJXtfcrU5f7VBK4pQL.5YMV11ne', '09075809581', 'Buru-un', 'Zone Taurus', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 16:32:03', '0000-00-00 00:00:00', 'customer', 0),
(16, 'Miriam Santiago', 'miriam.santiago@gmail.com', '$2y$10$G.5.TfSgILFl5yWTCiVU5u2yJ99iIrQRxS0TcHHM/TEYYr7TxU7lW', '09075809581', 'Tubod', 'Purok 4', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 16:41:28', '2015-11-29 20:00:03', 'customer', 0),
(17, 'Joseph Estrada', 'joseph.estrada@gmail.com', '$2y$10$aMF5KWDy6U20Vl.6yRsJlO5BGyU0rgC6jNV4XzJiQmHPdIX7fA3dC', '09075809581', 'Suarez', 'Zone Gemini', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 18:22:00', '2015-11-28 11:22:55', 'customer', 0),
(18, 'Wes Anderson', 'wes.anderson@gmail.com', '$2y$10$nEqYsLupaiWpjPWP6QoEne7bMShGrXgbOd0P0m8o/wByCqZi.qCGG', '09023475810', 'Tibanga', 'Purok 8', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-28 22:14:21', '2015-12-05 18:33:59', 'customer', 0),
(19, 'Maria Ozawa', 'maria.ozawa@gmail.com', '$2y$10$r7Jy9aOrgCYun3GAZqqQl.v46CUqiXMszcjA9QBlnD/f.tOsfT.SC', '', '', '', '', '', '', '', '2015-11-29 17:19:55', '2015-11-29 10:29:10', 'admin,editor', 0),
(20, 'Major Lazer', 'major.lazer@gmail.com', '$2y$10$vGB.hw4m5x/YwClOn1sy2eJuBFaCoeKqLk26L/w1ZN65VgTFD2966', '09075809581', 'Bara-as', 'Purok 3', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-30 00:20:36', '2015-11-29 17:21:45', 'customer', 0),
(21, 'Jackie Chan', 'jackie.chan@yahoo.com', '$2y$10$7xY5sxKAHf8FGDCDZw1gROUNbDmHV9peIM2DC3xBdgUct07kWEBEi', '09023475810', 'Fuentes', 'Purok 12', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-30 03:05:51', '2015-11-29 20:16:19', 'customer', 0),
(22, 'Son Guko', 'son.guko@gmail.com', '$2y$10$uVJUpQRn76ze/kDjHej7LOj7AQ1FmZYSbl0t/L/WfORDyJSXHqVkm', '09023475810', 'Canaway', 'Purok 15', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-11-30 08:49:55', '2015-11-30 01:51:30', 'customer', 0),
(23, 'Stephanie Espolong', 'stephanie.espolong@gmail.com', '$2y$10$1t/jjgKxSNYF6m45L8XxCuN/uQduDYWVPGDdV.kFwoaQWlT7IZHbq', '09158626694', 'Tibanga', 'Elena Tower', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-12-01 13:29:24', '2015-12-01 13:21:50', 'customer', 0),
(24, 'Gene Manlimos', 'gene.manlimos@gmail.com', '$2y$10$e44QBogxgwA563VeaAybGOXDb6nQ45jZMG0i/6sgUDmeJq8GAFxEu', '09023475810', 'San Miguel', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-12-01 23:53:45', '2015-12-01 17:21:16', 'customer', 0),
(25, 'Pope Francis', 'pope.francis@gmail.com', '$2y$10$9ehpMQ0QZd7CBrAnyfQKSescnLVGxqR.hj8e9LWHfzsTINVh06mWO', '09075809581', 'Tibanga', 'West Bend', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-12-02 19:39:41', '2015-12-02 12:40:08', 'customer', 0),
(26, 'Adam Levigne', 'adam.levigne@gmail.com', '$2y$10$qX14upgiL0e2Ssl9GaNyJ.LICnTUkIQsueZO9NKNRUk6n1BQwUAhO', '09023475810', 'Tominobo', 'Purok 10', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-12-02 20:01:09', '2015-12-02 13:09:42', 'customer', 0),
(27, 'Jewell Manlimos', 'info@gmail.com', '$2y$10$vwIqYMQa3ELPuHNQWJWIDOyeOsz3HAT7OlQAXjJEQ1HrZtLajJhEy', '09201920321', 'iligan city', 'door 2', 'iligan city', 'Lanao del Norte', '9200', 'Philippines', '2015-12-03 10:58:38', '2015-12-03 03:59:09', 'customer', 0),
(28, 'Eden Ibit', 'edenmae@gmail.com', '$2y$10$pIX.D5Vb/Q4rgIF6XPMtE.LWKi1w7K/926m.EE5Pb5jcHyNNw8ITm', '12131231231', 'ddakjsdh', 'kjhsjdkfhskjdf', '131sdkjsd', 'sdsdfsdfsf', '3e29374', 'Philippines', '2015-12-03 14:49:18', '2015-12-03 07:49:39', 'customer', 0),
(29, 'mayriele', 'gen@yahoo.com', '$2y$10$DdgSLtPgfVVbsrM6e7VvO.XS69YsB6z5QioWPxCiS3GujLWfzoi/e', '09287789131', '212 suarez', '', 'Iligan', 'Lanao', '9200', 'Philippines', '2015-12-03 17:57:20', '2015-12-03 10:57:39', 'customer', 0),
(30, 'Lorena ', 'lorenakarla22@gmail.com', '$2y$10$cd3zI2L4l4QmhOz8JZ0ROe.HImVl2D0Ueyb28zRMuNKg/Dh1blnGG', '0948312345', 'Tibanga', 'Mariquit', 'Iligan', 'Lanao del Norte', '9200', 'Philippines', '2015-12-04 12:28:34', '2015-12-04 05:29:00', 'customer', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
