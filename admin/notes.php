<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	if(!is_logged_in()){
		login_error_redirect();
	}
	if($user_data['permissions'] != 'admin,editor'){
		login_error_redirect();
	}
	
	include 'includes/head.php';
	include 'includes/navigation.php';
?>

<div class="container">
	<h3>Notes</h3>
	<ul>
		<li><strike>Sizes Checkbox</strike></li>
		<li>Tool Tip on Forms as User Guides</li>
		<li><strike>Shopping Cart</strike></li>
	</ul>
</div>