<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	if(!is_logged_in()){
		login_error_redirect();
	}
	if($user_data['permissions'] != 'admin,editor'){
		login_error_redirect();
	}
	include 'includes/head.php';
	include 'includes/navigation.php';
	$product_archived_result = $conn->query("SELECT * FROM products WHERE archived = 1");

	if(isset($_GET['unarchived'])){
		$archivedID = (int)$_GET['id'];
		$archived = (int)$_GET['unarchived'];
		$conn->query("UPDATE products SET archived='$archived' WHERE id = '$archivedID'");
		header('Location: archived.php');
	}
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h3 class="pull-left"><a class="text-muted" href="products.php">Product List</a></h3><h3 class="pull-left">&nbsp;|&nbsp;</h3><h3 class="pull-left">Archived List</h3>
			<div class="clearfix"></div>
			
			<div class="table-responsive">
				<table class="table table-striped">
					<thead>
						<th>Product</th><th>Price</th><th>Category</th><th>Actions</th>
					</thead>
					
					<tbody>
						<?php
						while($product = mysqli_fetch_assoc($product_archived_result)) :
							$childID = $product['categories'];
							$catSql = "SELECT * FROM categories WHERE id = '$childID'";
							$result = $conn->query($catSql);
							$child = mysqli_fetch_assoc($result);
							$parentID = $child['parent'];
							$pSql = "SELECT * FROM categories WHERE id = '$parentID'";
							$presult = $conn->query($pSql);
							$parent = mysqli_fetch_assoc($presult);
							$category = $parent['category'].', '.$child['category'];
						?>
						<tr>
							<td class="text-muted"><?php echo $product['name']; ?></td>
							<td class="text-muted"><?php echo money($product['price']); ?></td>
							<td class="text-muted"><?php echo $category; ?></td>
							<td>
								<button type="button" class="btn btn-sm btn-default-grey" onclick="detailsModal(<?php echo $product['id']; ?>)" title="View Details"><span class="glyphicon glyphicon-zoom-in"></span> Details</button>
								<a href="archived.php?unarchived=<?php echo (($product['unarchived'] == 1)?'0':'1'); ?>&id=<?php echo $product['id']; ?>" class="btn btn-sm btn-default-grey" title="Unarchived Product"><span class="glyphicon glyphicon-refresh"></span> Unarchived</a>
								<button type="button" class="btn btn-sm btn-default-grey" onclick="confirmDelete(<?php echo $product['id']; ?>)"><span class="glyphicon glyphicon-remove"></span> Remove</button>
							</td>
						</tr>
						<?php endwhile; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




<?php include 'includes/footer.php'; ?>