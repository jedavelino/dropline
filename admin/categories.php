<?php
	require_once '../core/init.php';
	if(!is_logged_in()){
		login_error_redirect();
	}
	if($user_data['permissions'] != 'admin,editor'){
		login_error_redirect();
	}
	include 'includes/head.php';
	include 'includes/navigation.php';
	
	$parent_result = $conn->query("SELECT * FROM categories WHERE parent = 0");
	$errors = array();
	$category = '';
	$post_parent = '';

	// Edit category
	if (isset($_GET['edit']) && !empty($_GET['edit'])) {
		$editID = (int)$_GET['edit'];
		$editID = sanitize($editID);
		$edit_result = $conn->query("SELECT * FROM categories WHERE id = '$editID'");
		$edit_category = mysqli_fetch_assoc($edit_result);
	}
	
	// Delete category
	if (isset($_GET['delete']) && !empty($_GET['delete'])) {
		$deleteID = (int)$_GET['delete'];
		$deleteID = sanitize($deleteID);

		// Delete orphaned item, an orphaned item is an item which its parents no longer exist or have been deleted
		$result = $conn->query("SELECT * FROM categories WHERE id = '$deleteID'");
		$category = mysqli_fetch_assoc($result);
		if ($category['parent'] == 0) {
			$conn->query("DELETE FROM categories WHERE parent = '$deleteID'");
		}

		$conn->query("DELETE FROM categories WHERE id = '$deleteID'");
		header('Location: categories.php');
	}
	
	// Process Form
	if(isset($_POST) && !empty($_POST)) {
		$post_parent = sanitize($_POST['parent']);
		$category = sanitize($_POST['category']);
		$sqlform = "SELECT * FROM categories WHERE category = '$category' AND parent = '$post_parent'";
		
		if(isset($_GET['edit'])) {
			$id = $edit_category['id'];
			$sqlform = "SELECT * FROM categories WHERE category = '$category' AND parent = '$post_parent' AND id != '$id'";
		}
		
		$fresult = $conn->query($sqlform);
		$count = mysqli_num_rows($fresult);
		
		// check if category is blank
		if($category == ''){
			$errors[] .= 'The category cannot be left blank.';
		}
		
		// check if the category already exist
		if($count > 0){
			$errors[] .= $category.' already exist! Please choose another category.';
		}
		
		// display errors or update database
		if(!empty($errors)){
			$display = display_errors($errors); ?>
		
		<script type="text/javascript">
			jQuery('document').ready(function() {
				jQuery('#errors').html('<?php echo $display; ?>');
			});
		</script>
		<?php } else {
			$update_db = "INSERT INTO categories (category,parent) VALUES ('{$category}','{$post_parent}')";
			if (isset($_GET['edit'])) {
				$update_db = "UPDATE categories SET category = '{$category}', parent = '{$post_parent}' WHERE id = '{$editID}'";
			}
			$conn->query($update_db);
			header('Location: categories.php');
		}
	}
	
	$category_value = '';
	$parent_value = 0;

	if (isset($_GET['edit'])) {
		$category_value = $edit_category['category'];
		$parent_value = $edit_category['parent'];
	} else {
		if (isset($_POST)) {
			$category_value = $category;
			$parent_value = $post_parent;
		}
	}
?>	

<div class="container" style="margin-top:20px;">
	<div class="row"><div class="col-md-8 col-md-offset-2"><div id="errors"></div></div></div>
	<div class="row">
		<div class="col-md-3 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading text-center"><h4><?php echo ((isset($_GET['edit']))?'Edit':'Add'); ?> Category</h4></div>
				<div class="panel-body">
				<div class="text-center" id="errors"></div>
					<form action="categories.php<?php echo ((isset($_GET['edit']))?'?edit='.$editID:''); ?>" name="categories" method="post">
						<div class="form-group">
							<label for="parent">Parent</label>
							<select class="form-control" name="parent" id="parent">
								<option value="0"<?php echo (($parent_value == 0)?' selected="selected"':''); ?>>Parent</option>
								<?php while ($parent = mysqli_fetch_assoc($parent_result)) : ?>
								<option value="<?php echo $parent['id']; ?>"<?php echo (($parent_value == $parent['id'])?' selected="selected"':''); ?>><?php echo $parent['category']; ?></option>
								<?php endwhile; ?>
							</select>
						</div>
						
						<div class="form-group">
							<label for="category">Category</label>
							<input type="text" class="form-control" id="category" name="category" value="<?php echo $category_value; ?>" required>
						</div>
						
						<div class="form-group">
							<?php if(isset($_GET['edit'])) : ?>
							<a class="btn btn-default-grey" href="categories.php">Cancel</a>
							<?php endif; ?>
							<button type="submit" class="btn btn-<?php echo ((isset($_GET['edit'])?'':'block')); ?> btn-success"><?php if(isset($_GET['edit'])){echo 'Save Changes';}else{echo '<span class="glyphicon glyphicon-plus"></span>'.' '.'Add Category';} ?></button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		<div class="col-md-5">
			<div class="panel panel-default">
				<div class="panel-heading text-center"><h4>Categories</h4></div>
				<div class="table-responsive">
					<table class="table table-bordered">
						<tbody>
							<?php
							$parent_result = $conn->query("SELECT * FROM categories WHERE parent = 0");
							
							while ($parent = mysqli_fetch_assoc($parent_result)) :
								$parentID = (int)$parent['id'];
								$child_result = $conn->query("SELECT * FROM categories WHERE parent = '$parentID'");
							?>
							<tr class="active">
								<td class="text-uppercase"><strong><?php echo $parent['category']; ?></strong></td>
								<td>
									<a href="categories.php?edit=<?php echo $parent['id']; ?>" class="btn btn-sm btn-default btn-default-grey"><span class="glyphicon glyphicon-cog"></span> Edit</a>
									<a href="categories.php?delete=<?php echo $parent['id']; ?>" class="btn btn-sm btn-default btn-default-grey"><span class="glyphicon glyphicon-trash"></span> Remove</a>
								</td>
							</tr>
								<?php while ($child = mysqli_fetch_assoc($child_result)) : ?>
								<tr>
									<td><?php echo $child['category']; ?></td>
									<td>
										<a href="categories.php?edit=<?php echo $child['id']; ?>" class="btn btn-sm btn-default btn-default-grey"><span class="glyphicon glyphicon-cog"></span> Edit</a>
										<a href="categories.php?delete=<?php echo $child['id']; ?>" class="btn btn-sm btn-default btn-default-grey"><span class="glyphicon glyphicon-trash"></span> Remove</a>
									</td>
								</tr>
								<?php endwhile; ?>
							<?php endwhile; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>



<?php include 'includes/footer.php'; ?>