<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	if(!is_logged_in()){
		login_error_redirect();
	}
	if($user_data['permissions'] != 'admin,editor'){
		login_error_redirect();
	}
	include 'includes/head.php';
	include 'includes/navigation.php';
	$deactivated_users_result = $conn->query("SELECT * FROM users WHERE deactivated = 1");

	if(isset($_GET['activate'])){
		$activatedID = (int)$_GET['id'];
		$activated = (int)$_GET['activate'];
		$conn->query("UPDATE users SET deactivated='$activated' WHERE id = '$activatedID'");
		header('Location: deactivated_users.php');
	}
?>

<div class="container">
	<h3 class="pull-left"><a class="text-muted" href="users.php">User List</a></h3><h3 class="pull-left">&nbsp;|&nbsp;</h3><h3 class="pull-left">Deactivated User List</h3>
	<div class="clearfix"></div>

	<table class="table table-striped">
		<thead>
			<th>Name</th><th>Email</th><th>Date Joined</th><th>Last Login</th><th>Privileges</th><th></th>
		</thead>
		
		<tbody>
		<?php while($user = mysqli_fetch_assoc($deactivated_users_result)) :
		
		?>
			<tr>
				<td class="text-muted"><?php echo $user['full_name']; ?></td>
				<td class="text-muted"><?php echo $user['email']; ?></td>
				<td class="text-muted"><?php echo pretty_date($user['join_date']); ?>
				<td class="text-muted"><?php echo (($user['last_login'] == '0000-00-00 00:00:00')?'Not Yet Logged':pretty_date($user['last_login'])); ?>
				<td class="text-muted"><?php echo $user['permissions']; ?>
				<td><a href="deactivated_users.php?activate=<?php echo (($user['deactivated'] == 1)?'0':'1'); ?>&id=<?php echo $user['id']; ?>" class="btn btn-sm btn-default btn-default-grey"><span class="glyphicon glyphicon-repeat"></span> Activate</a></td>
			</tr>
		<?php endwhile; ?>
		</tbody>
	</table>
</div>






<?php include 'includes/footer.php'; ?>