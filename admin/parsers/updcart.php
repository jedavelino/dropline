<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	$mode = sanitize($_POST['mode']);
	$eid = sanitize($_POST['eid']);
	$esize = sanitize($_POST['esize']);
	$cartq = $conn->query("SELECT * FROM cart WHERE id = '{$cartID}'");
	$result = mysqli_fetch_assoc($cartq);
	$items = json_decode($result['items'], true);
	$updated_items = array();
	$domain = (($_SERVER['HTTP_HOST'] != 'localhost')?'.'.$SERVER['HTTP_HOST']:false);

	if($mode == 'sub'){
		foreach($items as $item){
			if($item['id'] == $eid && $item['size'] == $esize){
				$item['quantity'] = $item['quantity'] - 1;
			}
			if($item['quantity'] > 0){
				$updated_items[] = $item;
			}
		}
	}

	if($mode == 'add'){
		foreach($items as $item){
			if($item['id'] == $eid && $item['size'] == $esize){
				$item['quantity'] = $item['quantity'] + 1;
			}
			$updated_items[] = $item;
		}
	}

	if(!empty($updated_items)){
		$json_updated = json_encode($updated_items);
		$conn->query("UPDATE cart SET items = '{$json_updated}' WHERE id = '{$cartID}'");
		$_SESSION['success_flash'] = 'Your shopping cart has been updated.';
	}

	if(empty($updated_items)){
		$conn->query("DELETE FROM cart WHERE id = '{$cartID}'");
		setcookie(CART_COOKIE, '', 1, "/", $domain, false); 
	}
?>