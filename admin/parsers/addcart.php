<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	$pID = sanitize($_POST['pid']);
	$size = sanitize($_POST['size']);
	$quantity = sanitize($_POST['quantity']);
	$item = array();
	$item[] = array(
		'id' => $pID,
		'size' => $size,
		'quantity' => $quantity
	);
	$domain = (($_SERVER['HTTP_HOST'] != 'localhost')?'.'.$_SERVER['HTTP_HOST']:false);
	$result = $conn->query("SELECT * FROM products WHERE id = '{$pID}'");
	$product = mysqli_fetch_assoc($result);
	$_SESSION['success_flash'] = $product['name']. ' was successfully added to your cart.';

	// Check cart cookie
	if($cartID != ''){
		$cartQ = $conn->query("SELECT * FROM cart WHERE id = '{$cartID}'");
		$cart = mysqli_fetch_assoc($cartQ);
		$previous_items = json_decode($cart['items'], true);
		$item_match = 0;
		$new_items = array();
		foreach($previous_items as $pitem){
			if($item[0]['id'] == $pitem['id'] && $item[0]['size'] == $pitem['size']){
				$pitem['quantity'] = $pitem['quantity'] + $item[0]['quantity'];
				$item_match = 1;
			}
			$new_items[] = $pitem;
		}
		if($item_match != 1){
			$new_items = array_merge($item, $previous_items);
		}

		$items_json = json_encode($new_items);
		$cart_expire = date("Y-m-d H:i:s", strtotime("+30 days"));
		$conn->query("UPDATE cart SET items = '{$items_json}', expire_date = '{$cart_expire}' WHERE id = '{$cartID}'");
		setcookie(CART_COOKIE, '', 1, "/", $domain, false);
		setcookie(CART_COOKIE, $cartID, CART_COOKIE_EXPIRE, '/', $domain, false);
	} else {
		// Add cart to the database
		$items_json = json_encode($item);
		$cart_expire = date("Y-m-d H:i:s", strtotime("+30 days"));
		$conn->query("INSERT INTO cart (items, expire_date) VALUES ('{$items_json}', '{$cart_expire}')");
		$cartID = $conn->insert_id;
		setcookie(CART_COOKIE, $cartID, CART_COOKIE_EXPIRE, '/', $domain, false);
	}
?>