<?php
	require_once '../core/init.php';
	if(!is_logged_in()){
		login_error_redirect();
	}
	if($user_data['permissions'] != 'admin,editor'){
		login_error_redirect();
	}
	if(!has_permission('admin')){
		permission_error_redirect('index.php');
	}
	include 'includes/head.php';
	include 'includes/navigation.php';
	$per_page = 5;
	// delete user
	if(isset($_GET['delete'])){
	$delete_user_id = sanitize($_GET['delete']);
	$conn->query("DELETE FROM users WHERE id = '$delete_user_id'");
	//$_SESSION['success_flash'] = '<span class="glyphicon glyphicon-ok-sign"></span> The user has been deleted.';
	header('Location: users.php');
	}
	
	if(isset($_GET['add']) || isset($_GET['edit'])) {
		$full_name = ((isset($_POST['full_name']) && $_POST['full_name'] != '')?sanitize($_POST['full_name']):'');
		$email = ((isset($_POST['email']) && $_POST['email'] != '')?sanitize($_POST['email']):'');
		$permissions = ((isset($_POST['permissions']) && $_POST['permissions'] != '')?sanitize($_POST['permissions']):'');
		$password = ((isset($_POST['password']) && $_POST['password'] != '')?sanitize($_POST['password']):'');
		$confirm_password = ((isset($_POST['confirm_password']) && $_POST['confirm_password'] != '')?sanitize($_POST['confirm_password']):'');
		
		if(isset($_GET['edit'])) {
			$editID = (int)$_GET['edit'];
			$edit_user_result = $conn->query("SELECT * FROM users WHERE id = '$editID'");
			$user = mysqli_fetch_assoc($edit_user_result);
			
			$full_name = ((isset($_POST['full_name']) && $_POST['full_name'] != '')?sanitize($_POST['full_name']):$user['full_name']);
			$email = ((isset($_POST['email']) && $_POST['email'] != '')?sanitize($_POST['email']):$user['email']);
			$permissions = ((isset($_POST['permissions']) && $_POST['permissions'] != '')?sanitize($_POST['permissions']):$user['permissions']);
		}
		
		// add user
		if($_POST){
		$errors = array();
		
		// checking if email already exist
		$user_query = $conn->query("SELECT * FROM users WHERE email = '$email'");
		$user = mysqli_fetch_assoc($user_query);
		$user_count = mysqli_num_rows($user_query);
		if($user_count != 0){
			$errors[] = 'That email already exist.';
		}
		
		if(strlen($password) < 6){
			$errors[] = 'Passwords must be at least 6 characters.';
		}
		
		if($password != $confirm_password){
			$errors[] = 'Passwords do not match.';
		}
		
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			$errors[] = 'Enter a valid email.';
		}
		
		if(!empty($errors)){
			echo display_errors($errors);
		} else {
			// add user
			$hashed_password = password_hash($password, PASSWORD_DEFAULT);
			$insert_query = "INSERT INTO users (full_name, email, password, permissions) VALUES ('{$full_name}', '{$email}', '{$hashed_password}', '{$permissions}')";
			//$_SESSION['success_flash'] = '<span class="glyphicon glyphicon-ok-sign"></span> A user has been added.';
			
			if(isset($_GET['edit'])) {
				$insert_query = "UPDATE users SET full_name = '{$full_name}', email = '{$email}', permissions = '{$permissions}' WHERE id = '{$editID}'";
			}
			$conn->query($insert_query);
			header('Location: users.php');
		}
		
	}
?>

<div class="container">
	<h3 class="page-header"><?php echo ((isset($_GET['edit']))?'Edit':'Add'); ?> User</h3>
	<form action="users.php?<?php echo ((isset($_GET['edit']))?'edit='.$editID:'add=1'); ?>" method="post">
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label for="full_name">Name</label>
					<input type="text" class="form-control" id="full_name" name="full_name" value="<?php echo $full_name; ?>" required>
				</div>
			
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>" required>
				</div>
			
				<div class="form-group">
					<label for="permissions">Privileges</label>
					<select class="form-control" name="permissions" id="permissions" required>
						<option value=""<?php echo (($permissions == '')?' selected':''); ?>></option>
						<option value="admin,editor"<?php echo (($permissions == 'admin,editor')?' selected':''); ?>>Admin</option>
						<option value="editor"<?php echo (($permissions == 'editor')?' selected':''); ?>>Editor</option>
					</select>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" class="form-control" name="password" id="password" value="<?php echo $password; ?>" required <?php echo ((isset($_GET['edit']))?'readonly':''); ?>>
				</div>
			
				<div class="form-group">
					<label for="confirm_password">Confirm Password</label>
					<input type="password" class="form-control" name="confirm_password" id="confirm_password" value="<?php echo $confirm_password; ?>" required <?php echo ((isset($_GET['edit']))?'readonly':''); ?>>
				</div>
			</div>
		</div>
		
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<a href="users.php" class="btn btn-default">Cancel</a>
					<input type="submit" name="submit" class="btn btn-success" value="<?php echo ((isset($_GET['edit']))?'Save Changes':'Add User'); ?>">
				</div>
			</div>
		</div>
	</form>
</div>

<?php } else {

	$page = ((isset($_GET['page']))?$_GET['page']:1);
	$start_from = ($page-1)*$per_page;


	$users_result = $conn->query("SELECT * FROM users WHERE deactivated = 0 LIMIT $start_from, $per_page");
	
	if(isset($_GET['deactivated'])){
		$deactivatedID = (int)$_GET['id'];
		$deactivated = (int)$_GET['deactivated'];
		$conn->query("UPDATE users SET deactivated='$deactivated' WHERE id = '$deactivatedID'");
		//$conn->query("UPDATE users SET featured = 0 WHERE id = '$archivedID'");
		header('Location: users.php');
	}
?>		
		
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!--<h2 class="pull-left">User List</h2><div class="pull-right"><a href="users.php?add=1" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add User</a> <a href="deactivated_users.php" class="btn btn-default"><span class="glyphicon glyphicon-lock"></span> Deactivated</a></div>
			<div class="clearfix"></div>-->
			<h2>User List</h2>
			<div class="table-responsive">
				<table class="table table">
					<thead>
						<th>Name</th><th>Email</th><th>Date Joined</th><th>Last Login</th><th>Privileges</th><th>Action</th>
					</thead>
					
					<tbody>
					<?php while($user = mysqli_fetch_assoc($users_result)) : ?>
						<tr class="<?php echo (($user['id'] == $user_data['id'])?'active':''); ?>">
							<td><?php echo $user['full_name']; ?></td>
							<td><?php echo $user['email']; ?></td>
							<td><?php echo pretty_date($user['join_date']); ?></td>
							<td><?php echo (($user['last_login'] == '0000-00-00 00:00:00')?'Not Yet Logged':pretty_date($user['last_login'])); ?></td>
							<td><?php echo $user['permissions']; ?></td>
							<td>
								<!--<a href="users.php?edit=<?php //echo $user['id']; ?>" class="btn btn-sm btn-default-grey" title="Edit User"><span class="glyphicon glyphicon-edit"></span> Privilege</a>-->

								<a onclick="privilege_modal(<?php echo $user['id']; ?>);" class="btn btn-sm btn-default-grey" title="Change Privilege"><span class="glyphicon glyphicon-edit"></span> Privilege</a>
								
								<?php if($user['id'] != $user_data['id']) : ?>
								<a href="users.php?deactivated=<?php echo (($user['deactivated'] == 0)?'1':'0'); ?>&id=<?php echo $user['id']; ?>" class="btn btn-sm btn-default btn-default-grey" title="Deactivate User"><span class="glyphicon glyphicon-lock"></span> Deactivate</a>
								<?php endif; ?>
							</td>
						</tr>
					<?php endwhile; ?>
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-md-12">
			<?php
				$users_count = $conn->query("SELECT * FROM users WHERE deactivated = 0");
				$total_records = mysqli_num_rows($users_count);
				$total_pages = ceil($total_records/$per_page);
			?>
			<nav>
				<ul class="pager">
					<li><a href="users.php?page=1">First</a></li>
					<?php for($i=1;$i<=$total_pages;$i++) { ?>
					<li><a href="users.php?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
					<?php } ?>
					<li><a href="users.php?page=<?php echo $total_pages; ?>">Last</a></li>
				</ul>
			</nav>
		</div>
	</div>
</div>

<?php } include 'includes/footer.php'; ?>