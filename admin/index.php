<?php
	require_once '../core/init.php';

	if(!is_logged_in()){
		header('Location: login.php');
	}
	if($user_data['permissions'] != 'admin,editor'){
		//login_error_redirect();
		header('Location: login.php');
	}

	include 'includes/head.php';
	include 'includes/navigation.php';

	$transq = "SELECT t.id, t.cart_id, t.full_name, t.trans_date, t.ordertotal FROM transactions t LEFT JOIN cart c ON t.cart_id = c.id WHERE paid = 0 ORDER BY t.trans_date";
	$result = $conn->query($transq);

	$comp_transq = "SELECT t.id, t.cart_id, t.full_name, t.trans_date, t.ordertotal, t.completed_date FROM transactions t LEFT JOIN cart c ON t.cart_id = c.id WHERE paid = 1 ORDER BY t.completed_date";
	$comp_trans_result = $conn->query($comp_transq);

	// for pagination
	$count_transp0 = $conn->query("SELECT COUNT(id) FROM transactions WHERE paid = 0");
	$rowp0 = mysqli_fetch_row($count_transp0);
	$total_rowp0 = $rowp0[0];
	// specify how many results per page
	$rpp = 5;
	// this tells us the page number of our last page
	$last = ceil($total_rowp0/$rpp);
	// this makes sure that the last page "$last" cannot be less than 1
	if($last < 1){
		$last = 1;
	}
?>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h4>Pending Orders</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-condensed">
									<thead>
										<th>Order No.</th><th>Customer Name</th><th>Total</th><th>Date Ordered</th><th>Action</th>
									</thead>
									<tbody>
									<?php while($order = mysqli_fetch_assoc($result)) : ?>
										<tr>
											<td class="text-center"><?php echo $order['id']; ?></td>
											<td><?php echo $order['full_name']; ?></td>
											<td><?php echo money($order['ordertotal']); ?></td>
											<td><?php echo pretty_date2($order['trans_date']); ?></td>
											<td><button type="button" class="btn btn-xs btn-default-grey" onclick="order_details(<?php echo $order['id']; ?>);">Details</button></td>
										</tr>
									<?php endwhile; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h4>Completed Orders</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-condensed">
									<thead>
										<th>Order No.</th><th>Customer Name</th><th>Total</th><th>Date Completed</th><th>Action</th>
									</thead>
									<tbody>
									<?php while($order = mysqli_fetch_assoc($comp_trans_result)) : ?>
										<tr>
											<td class="text-center"><?php echo $order['id']; ?></td>
											<td><?php echo $order['full_name']; ?></td>
											<td><?php echo money($order['ordertotal']); ?></td>
											<td><?php echo pretty_date2($order['completed_date']); ?></td>
											<td><button type="button" class="btn btn-xs btn-default-grey" onclick="order_details(<?php echo $order['id']; ?>);">Details</button></td>
										</tr>
									<?php endwhile; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php include 'includes/footer.php'; ?>