<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	if(!is_logged_in()){
		login_error_redirect();
	}
	if($user_data['permissions'] != 'admin,editor'){
		login_error_redirect();
	}
	include 'includes/head.php';
	include 'includes/navigation.php';
	$db_path = '';
	
	if(isset($_GET['add']) || isset($_GET['edit'])){
		$parent_query = $conn->query("SELECT * FROM categories WHERE parent = 0 ORDER BY category");
		$name = ((isset($_POST['name']) && $_POST['name'] != '')?sanitize($_POST['name']):'');
		$sizes = ((isset($_POST['size']) && $_POST['size'] != '')?implode(',',$_POST['size']):'');
		$price = ((isset($_POST['price']) && $_POST['price'] != '')?sanitize($_POST['price']):'');
		$parent = ((isset($_POST['parent']) && !empty($_POST['parent']))?sanitize($_POST['parent']):'');
		$category = ((isset($_POST['child']) && !empty($_POST['child']))?sanitize($_POST['child']):'');
		$description = ((isset($_POST['description']) && $_POST['description'] != '')?sanitize($_POST['description']):'');
		$saved_image = '';
		
		if(isset($_GET['edit'])){
			$editID = (int)$_GET['edit'];
			$edit_product_result = $conn->query("SELECT * FROM products WHERE id='$editID'");
			$product = mysqli_fetch_assoc($edit_product_result);
			if(isset($_GET['delete_image'])){
				$imgi = (int)$_GET['imgi'] - 1;
				$images = explode(',',$product['image']);
				$image_url = $_SERVER['DOCUMENT_ROOT'].$images[$imgi];
				unlink($image_url);
				unset($images[$imgi]);
				$imageString = implode(',',$images);
				$product_delete_query = $conn->query("UPDATE products SET image = '{$imageString}' WHERE id = '{$editID}'");
				header('Location: products.php?edit='.$editID);
			}
			$category = ((isset($_POST['child']) && $_POST['child'] != '')?sanitize($_POST['child']):$product['categories']);
			$name = ((isset($_POST['name']) && $_POST['name'] != '')?sanitize($_POST['name']):$product['name']);
			$sizes = ((isset($_POST['size']) && $_POST['size'] != '')?implode(',',$_POST['size']):explode(',',$product['sizes']));
			$price = ((isset($_POST['price']))?sanitize($_POST['price']):$product['price']);
			$parent_q = $conn->query("SELECT * FROM categories WHERE id = '$category'");
			$parent_r = mysqli_fetch_assoc($parent_q);
			$parent = ((isset($_POST['parent']) && $_POST['parent'] != '')?sanitize($_POST['parent']):$parent_r['parent']);		
			$description = ((isset($_POST['description']))?sanitize($_POST['description']):$product['description']);
			$saved_image = (($product['image'] != '')?$product['image']:'');
			$db_path = $saved_image;
		}
		
		if($_POST){
			$errors = array();
			$required = array('name','price','parent','child');
			$allowed = array('jpg','png','jpeg','gif');
			$upload_path = array();
			$tmp_loc = array();
			foreach($required as $field){
				if($_POST[$field] == ''){
					$errors[] = 'Some fields cannot be left blank!';
					break;
				}
			}
			//var_dump($_FILES['photo']);
			$photo_count = count($_FILES['photo']['name']);
			if($photo_count > 0){
				for($i=0;$i<$photo_count;$i++){
					$img_name = $_FILES['photo']['name'][$i];
					$name_array = explode('.',$img_name);
					$file_name = $name_array[0];
					$file_ext = $name_array[1];
					$mime = explode('/',$_FILES['photo']['type'][$i]);
					$mime_type = $mime[0];
					$mime_ext = $mime[1];
					$tmp_loc[] = $_FILES['photo']['tmp_name'][$i];
					$file_size = $_FILES['photo']['size'][$i];
					$upload_name = md5(microtime().$i).'.'.$file_ext;
					$upload_path[] = BASEURL.'images/products/'.$upload_name;
					if($i != 0){
						$db_path .= ',';
					}
					$db_path .= '/dropline/images/products/'.$upload_name;
					if(!in_array($file_ext, $allowed)){
						$errors[] = 'The file extension must be a png, jpg, jpeg, or gif.';
					}
					if($file_size > 5000000){
						$errors[] = 'The file size must be under 5mb.';
					}
					// check for type
					if($mime_type != 'image') {
						$errors[] = 'The file must be an image.';
					}
					if($file_ext != $mime_ext && ($mime_ext == 'jpeg' && $file_ext != 'jpg')) {
						$errors[] = 'File extension does not match the file.';
					}
				}
			}
			
			if(!empty($errors)){
				echo display_errors($errors);
			} else {
				if($photo_count > 0){
					for($i=0;$i<$photo_count;$i++){
						move_uploaded_file($tmp_loc[$i], $upload_path[$i]);
					}
				}
				
				$update_db = "INSERT INTO products (name,price,sizes,categories,description,image) VALUES ('{$name}','{$price}','{$sizes}','{$category}','{$description}','{$db_path}')";
				if(isset($_GET['edit'])){
					$update_db = "UPDATE products SET name = '{$name}', price = '{$price}', sizes = '{$sizes}', categories = '{$category}', description ='{$description}', image ='{$db_path}' WHERE id ='{$editID}'";
				}
				$conn->query($update_db);
				header('Location: products.php');
			}
		}
?>

<div class="container">
	<form action="products.php?<?php echo ((isset($_GET['edit']))?'edit='.$editID:'add=1'); ?>" method="post" enctype="multipart/form-data">
		<div class="row">
			<!-- Product Details -->
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading text-center"><h4>Product Details</h4></div>
					<div class="panel-body">
						<div class="form-group">
							<label for="name">Name</label>
							<input type="text" class="form-control" id="name" name="name" value="<?php echo $name; ?>">
						</div>
						
						<div class="form-group">
							<label for="price">Price</label>
							<div class="input-group">
								<div class="input-group-addon">&#8369;</div>
								<input type="number" class="form-control" id="price" name="price" value="<?php echo $price; ?>" min="0">
							</div>
						</div>
						
						<div class="form-group">
							<label for="description">Description</label>
							<textarea class="form-control" name="description" id="description" rows="5" placeholder="Write a description..."><?php echo $description; ?></textarea>
						</div>
					</div>
				</div>
			</div>

			<!-- Product Category and Size -->
			<div class="col-md-4">
				<!-- Category -->
				<div class="panel panel-default">
					<div class="panel-heading text-center"><h4>Product Category</h4></div>
					<div class="panel-body">
						<div class="form-group">
							<label for="parent">Parent Category</label>
							<select class="form-control" name="parent" id="parent">
								<option value=""></option>
								<?php while($p = mysqli_fetch_assoc($parent_query)) : ?>
								<option value="<?php echo $p['id']; ?>"<?php echo (($parent == $p['id'])?' selected':''); ?>><?php echo $p['category']; ?></option>
								<?php endwhile; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="child">Child Category</label>
							<select class="form-control" id="child" name="child">
							</select>
						</div>
					</div>
				</div>

				<!-- Sizes -->
				<div class="panel panel-default">
					<div class="panel-heading text-center"><h4>Product Sizes</h4></div>
					<div class="panel-body">
						<label class="checkbox-inline">
							<input type="checkbox" name="size[]" value="Small" <?php if(isset($_GET['edit'])){ echo ((isset($sizes[0]) && $sizes[0] != '')?' checked':'');} ?>> Small
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" name="size[]" value="Medium" <?php if(isset($_GET['edit'])){ echo ((isset($sizes[1]) && $sizes[1] != '')?' checked':'');} ?>> Medium
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" name="size[]" value="Large" <?php if(isset($_GET['edit'])){ echo ((isset($sizes[2]) && $sizes[2] != '')?' checked':'');} ?>> Large
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" name="size[]" value="XL" <?php if(isset($_GET['edit'])){ echo ((isset($sizes[3]) && $sizes[3] != '')?' checked':'');} ?>> XL
						</label>
					</div>
				</div>
			</div>

			<!-- Product Image -->
			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-heading text-center"><h4>Product Image</h4></div>
					<div class="panel-body">
							<?php if($saved_image != '') : ?>
							<?php 
								$imgi = 1;
								$images = explode(',',$saved_image);
							?>
							<?php foreach($images as $image) : ?>
							<div class="col-xs-6 col-md-4">
								<div class="thumbnail">
									<p class="text-center"><?php echo 'Image '.$imgi; ?></p>
									<img class="img-responsive" src="<?php echo $image; ?>" alt="Saved Image"/>
									<div class="caption">
										<a href="products.php?delete_image=1&edit=<?php echo $editID; ?>&imgi=<?php echo $imgi; ?>" class="btn btn-xs btn-default btn-block">Delete</a>
									</div>
								</div>
							</div>
							<?php $imgi++; endforeach; ?>
								<?php //foreach($images as $image) : ?>
								
									<!--<div class="col-xs-4">
										<div class="thumbnail">
											<img class="img-not-responsive" src="<?php echo $image; ?>" alt="Saved Image"/>
										</div>
										<a href="products.php?delete_image=1&edit=<?php echo $editID; ?>&imgi=<?php echo $imgi; ?>" class="btn btn-xs btn-danger btn-block">Delete</a>
									</div>
								<?php //$imgi++; endforeach; ?>-->

						<?php else : ?>
						<div class="form-group">
							<label for="photo" class="sr-only">Image</label>
							<div class="input-group">
								<div class="input-group-addon"><span class="glyphicon glyphicon-picture"></span></div>
								<input type="file" name="photo[]" id="photo" class="form-control" multiple>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		
		<hr>
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<a href="products.php" class="btn btn-default-grey">Cancel</a>
					<input type="submit" name="submit" class="btn btn-success" value="<?php echo ((isset($_GET['edit']))?'Save Changes':'Add Product'); ?>">
				</div>					
			</div>
		</div>
	</form>
</div>
	
	<?php 
	} else {
		$product_result = $conn->query("SELECT * FROM products WHERE archived = 0");
		if(isset($_GET['featured'])){
			$featuredID = (int)$_GET['id'];
			$featured = (int)$_GET['featured'];
			$conn->query("UPDATE products SET featured='$featured' WHERE id = '$featuredID'");
			header('Location: products.php');
		}
		
		if(isset($_GET['archived'])){
			$archivedID = (int)$_GET['id'];
			$archived = (int)$_GET['archived'];
			$conn->query("UPDATE products SET archived='$archived' WHERE id = '$archivedID'");
			$conn->query("UPDATE products SET featured = 0 WHERE id = '$archivedID'");
			header('Location: products.php');
		}
	?>
	
<div class="container">
	<!--<h2 class="pull-left">Product List</h2><div class="pull-right"><a href="products.php?add=1" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Product</a> <a href="archived.php" class="btn btn-default"><span class="glyphicon glyphicon-folder-close"></span> Archived</a></div>
	<div class="clearfix"></div>-->
	<h2>Product List</h2>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<th>Product</th><th>Price</th><th>Category</th><th>Featured</th><th>Actions</th>
			</thead>
			
			<tbody>
				<?php
				while($product = mysqli_fetch_assoc($product_result)) :
					$childID = $product['categories'];
					$catSql = "SELECT * FROM categories WHERE id = '$childID'";
					$result = $conn->query($catSql);
					$child = mysqli_fetch_assoc($result);
					$parentID = $child['parent'];
					$pSql = "SELECT * FROM categories WHERE id = '$parentID'";
					$presult = $conn->query($pSql);
					$parent = mysqli_fetch_assoc($presult);
					$category = $parent['category'].', '.$child['category'];
				?>
				<tr>
					<td><?php echo $product['name']; ?></td>
					<td><?php echo money($product['price']); ?></td>
					<td><?php echo $category; ?></td>
					<td><a href="products.php?featured=<?php echo (($product['featured'] == 0)?'1':'0'); ?>&id=<?php echo $product['id']; ?>" class="btn btn-sm btn-default-grey"><span class="glyphicon glyphicon-<?php echo (($product['featured'] == 1)?'star':'star-empty'); ?>"></span> <?php echo (($product['featured'] == 1)?'Unfeatured':'Featured'); ?></a></td>
					<td><button type="button" class="btn btn-sm btn-default-grey" onclick="detailsModal(<?php echo $product['id']; ?>)" title="View Details"><span class="glyphicon glyphicon-zoom-in"></span> Details</button>
						<a href="products.php?edit=<?php echo $product['id']; ?>" class="btn btn-sm btn-default-grey" title="Edit Product"><span class="glyphicon glyphicon-cog"></span> Edit</a>
						<a href="products.php?archived=<?php echo (($product['archived'] == 0)?'1':'0'); ?>&id=<?php echo $product['id']; ?>" class="btn btn-sm btn-default-grey" title="Archived Product"><span class="glyphicon glyphicon-folder-close"></span> Archive</a>
					</td>
				</tr>
				<?php endwhile; ?>
			</tbody>
		</table>
	</div>
</div>

<?php } include 'includes/footer.php'; ?>

<script type="text/javascript">
	jQuery('document').ready(function() {
		get_child_options('<?php echo $category; ?>');
	});
</script>