<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	$productID = $_POST['id'];
?>
<?php ob_start(); ?>

<div class="modal" id="confirm_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="closeModal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="myModalLabel">Permanently Delete</h4>
			</div>
			<div class="modal-body">
				Are you sure that you want to permanently delete the product?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" onclick="closeModal()">Cancel</button>
				<button type="button" class="btn btn-danger" onclick="deleteProduct(<?php echo $productID; ?>)">Remove</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function closeModal() {
		jQuery('#confirm_modal').modal('hide');
		setTimeout(function(){
			jQuery('#confirm_modal').remove();
			jQuery('.modal-backdrop').remove();
		});
	}
</script>

<?php echo ob_get_clean(); ?>