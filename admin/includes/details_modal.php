<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	global $conn;
	$productID = $_POST['id'];
	$productID = (int)$productID;
	$product_result = $conn->query("SELECT * FROM products WHERE id = '$productID'");
	$product = mysqli_fetch_assoc($product_result);
	
	$childID = $product['categories'];
	$catSql = "SELECT * FROM categories WHERE id = '$childID'";
	$result = $conn->query($catSql);
	$child = mysqli_fetch_assoc($result);
	
	$parentID = $child['parent'];
	$pSql = "SELECT * FROM categories WHERE id = '$parentID'";
	$presult = $conn->query($pSql);
	$parent = mysqli_fetch_assoc($presult);

	$category = $parent['category'].' ('.$child['category'].')';
?>
<?php ob_start(); ?>

<!-- Modal -->
<div class="modal fade" id="details_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" onclick="closeModal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        	<h4 class="modal-title text-center" id="myModalLabel"><?php echo $product['name']; ?></h4>
	      	</div>
	      	<div class="modal-body">
	        	<div class="row">
					<div class="col-md-5">
						<div class="panel panel-default">
							<div class="panel-heading">Image Preview</div>
							<div class="panel-body">
								<div class="fotorama">
									<?php $photos = explode(',',$product['image']); ?>
									<img class="img-responsive" src="<?php echo $photos['0']; ?>" alt="<?php echo $product['name']; ?>">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7">
						<div class="panel panel-default">
							<div class="panel-heading">Details</div>
							<ul class="list-group">
								<li class="list-group-item"><strong>Product</strong>:&nbsp;&nbsp;<?php echo $product['name']; ?></li>
								<li class="list-group-item"><strong>Price</strong>:&nbsp;&nbsp;<?php echo money($product['price']); ?></li>
								<li class="list-group-item"><strong>Sizes</strong>:&nbsp;&nbsp;<?php echo $product['sizes'] ?></li>
								<li class="list-group-item"><strong>Category</strong>:&nbsp;&nbsp;<?php echo $category; ?></li>
								<?php if($product['description'] != '') : ?>
								<li class="list-group-item">
									<strong>Description</strong>:&nbsp;&nbsp;<br>
									<?php echo $product['description']; ?>
								</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
				</div>
	      	</div>
	      	<div class="modal-footer">
	        	<button type="button" class="btn btn-default" onclick="closeModal()">Close</button>
	      	</div>
    	</div>
  	</div>
</div>

<script type="text/javascript">
	function closeModal() {
		jQuery('#details_modal').modal('hide');
		setTimeout(function(){
			jQuery('#details_modal').remove();
			jQuery('.modal-backdrop').remove();
		});
	}
</script>


<?php echo ob_get_clean(); ?>