<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	$uID = sanitize($_POST['id']);
	$uID = (int)$uID;
	$result = $conn->query("SELECT * FROM users WHERE id = '{$uID}'");
	$user = mysqli_fetch_assoc($result);
	$permissions = $user['permissions'];

?>
<?php ob_start(); ?>
<!-- Modal -->
<div class="modal fade" id="privilege_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="myModalLabel">Change User Privilege</h4>
			</div>
			<div class="modal-body">
				<form action="change_privilege.php" method="post" id="change_privilege">
					<input type="hidden" name="uid" value="<?php echo $uID; ?>";?>
					<div class="form-group">
						<label for="permissions">Privileges</label>
						<select class="form-control" name="permissions" id="permissions" required>
							<option value=""<?php echo (($permissions == '')?' selected':''); ?>></option>
							<option value="admin,editor"<?php echo (($permissions == 'admin,editor')?' selected':''); ?>>Admin</option>
							<option value="editor"<?php echo (($permissions == 'editor')?' selected':''); ?>>Editor</option>
						</select>
					</div>
				<!--</form>-->
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" onclick="change_privilege();return false;" class="btn btn-primary">Change</button>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	function closeModal() {
		jQuery('#privilege_modal').modal('hide');
		setTimeout(function() {
			jQuery('#privilege_modal').remove();
			jQuery('.modal-backdrop').remove();
		},500);
	}
</script>

<?php echo ob_get_clean(); ?>