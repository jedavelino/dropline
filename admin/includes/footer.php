<!-- Footer -->
<br><br>
<footer class="footer">
	<div class="container">
		<h5 class="text-center">Copyright &copy; 2015 Dropline Co.</h5>
	</div>
</footer>

<script>

	function order_details(id){
		var data = {"id" : id};
		jQuery.ajax({
			url : '/dropline/admin/includes/order_details.php',
			method : "post",
			data : data,
			success : function(data){
				jQuery('body').append(data);
				jQuery('#order_details').modal('toggle');
			},
			error : function(){
				alert("Something went wrong.");
			}
		});
	}
	
	// Details modal
	function detailsModal(id) {
		var data = {"id" : id};
		jQuery.ajax({
			url : '/dropline/admin/includes/details_modal.php',
			method : "post",
			data : data,
			success : function(data) {
				jQuery('body').append(data);
				jQuery('#details_modal').modal('toggle');
			},
			error : function() {
				alert("Something went wrong.");
			}
		});
	}

	// a script to listen and select the children
	function get_child_options(selected) {
		if(typeof selected === 'undefined'){
			var selected = '';
		}
		var parentID = jQuery('#parent').val();
		jQuery.ajax({
			url: '/dropline/admin/parsers/child_categories.php',
			type: 'POST',
			data: {parentID : parentID, selected: selected},
			success: function(data){
				jQuery('#child').html(data);
			},
			error: function(){alert("Something went wrong with the child options.")},
		});
	}

	jQuery('select[name="parent"]').change(function() {
		get_child_options();
	});// listens which parent the user chooses
	
	// Confirm modal
	function confirmDelete(id) {
		var data = {"id" : id};
		jQuery.ajax({
			url : '/dropline/admin/includes/confirm_modal.php',
			method : "post",
			data: data,
			success : function(data) {
				jQuery('body').append(data);
				jQuery('#confirm_modal').modal('toggle');
			},
			error : function() {
				alert("Something went wrong.");
			}
		});
	}
	
	// Delete product
	function deleteProduct(id) {
		var data = {"id" : id};
		jQuery.ajax({
			url : '/dropline/admin/parsers/delete.php',
			method : "post",
			data : data,
			success : function() {
				location.reload();
			},
			error : function() {
				alert("Something went wrong.");
			}
		});
	}

	function complete_order(id){
		var data = {"id" : id};
		jQuery.ajax({
			url : '/dropline/admin/parsers/complete_order.php',
			method : "post",
			data: data,
			success : function(){
				location.reload();
			},
			error : function(){
				alert("Something went wrong.");
			}
		});
	}

	function privilege_modal(id){
		var data = {"id" : id};
		jQuery.ajax({
			url : '/dropline/admin/includes/privilege_modal.php',
			method : 'post',
			data : data,
			success : function(data){
				jQuery('body').append(data);
				jQuery('#privilege_modal').modal('toggle');
			},
			error : function(){
				alert("Something went wrong.");
			}
		});
	}

	function change_privilege(id){
		var data = {"id" : id};
		jQuery.ajax({
			url : '/dropline/admin/parsers/change_privilege.php',
			method : "post",
			data: data,
			success : function(){
				location.reload();
			},
			error : function(){
				alert("Something went wrong.");
			}
		});
	}

	function change_privilege(){
		var data = jQuery('#change_privilege').serialize();
			jQuery.ajax({
				url : '/dropline/admin/parsers/change_privilege.php',
				method : 'post',
				data : data,
				success : function(){
					location.reload();
					//window.location.href = '/dropline/cart.php';
				},
				error : function(){
					alert("Something went wrong!");
				}
			});
	}
	
</script>


</body>


</html>