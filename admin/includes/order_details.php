<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	global $conn;
	$orderID = $_POST['id'];
	$orderID = (int)$orderID;
	$trans_query = $conn->query("SELECT * FROM transactions WHERE id = '{$orderID}'");
	$trans = mysqli_fetch_assoc($trans_query);

	$cart_id = $trans['cart_id'];
	$cart_query = $conn->query("SELECT * FROM cart WHERE id = '{$cart_id}'");
	$cart = mysqli_fetch_assoc($cart_query);

	$items = json_decode($cart['items'], true);
	$id_array = array();
	$products = array();
	foreach($items as $item){
		$id_array[] = $item['id'];
	}
	$ids = implode(',',$id_array);
	$product_query = $conn->query("SELECT i.id as 'id', i.name as 'name', c.id as 'cid', c.category as 'child', p.category as 'parent' FROM products i LEFT JOIN categories c ON i.categories = c.id LEFT JOIN categories p ON c.parent = p.id WHERE i.id IN ({$ids})");
	while($p = mysqli_fetch_assoc($product_query)){
		foreach($items as $item){
			if($item['id'] == $p['id']){
				$x = $item;
				continue;
			}
		}
		$products[] = array_merge($x, $p);
	}
?>
<?php ob_start(); ?>

<div class="modal fade" id="order_details" tabindex="-1" role="dialog" aria-labelledby="orderDetailsModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" onclick="closeModal()" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title text-center" id="orderDetailsModalLabel">Order Details</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs nav-justified">
							<li role="presentation" class="active"><a data-toggle="tab" href="#items-ordered">Items Ordered</a></li>
							<li role="presentation"><a data-toggle="tab" href="#order-details">Order Details</a></li>
							<li role="presentation"><a data-toggle="tab" href="#shipping-address">Customer Details</a></li>
						</ul>
					</div>

					<div class="tab-content">
						<div class="col-md-12 tab-pane fade in active" id="items-ordered">
							<div class="panel panel-default">
								<div class="panel-heading text-center">Items Ordered</div>
								<div class="panel-body">
									<table class="table table-condensed">
										<thead>
											<th>Product</th><th>Quantity</th><th>Category</th><th>Size</th>
										</thead>
										<tbody>
										<?php foreach($products as $product) : ?>
											<tr>
												<td><?php echo $product['name']; ?></td>
												<td><?php echo (($product['quantity'] > 1)?$product['quantity'].' items':$product['quantity'].' item'); ?></td>
												<td><?php echo $product['parent'].', '.$product['child']; ?></td>
												<td><?php echo $product['size']; ?></td>
											</tr>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="col-md-12 tab-pane fade" id="order-details">
							<div class="panel panel-default">
								<div class="panel-heading text-center">Order Details</div>
								<div class="panel-body">
									<table class="table">
										<tbody>
											<tr>
												<td><strong>Status</strong></td>
												<?php if($trans['paid'] == 0) : ?>
												<td class="text-danger">Unpaid</td>
												<?php else : ?>
												<td class="text-success">Paid & Delivered</td>
												<?php endif ;?>
											</tr>
											<tr>
												<td><strong>Subtotal</strong></td>
												<td><?php echo money($trans['subtotal']); ?></td>
											</tr>
											<tr>
												<td><strong>Order Total</strong></td>
												<td><?php echo money($trans['ordertotal']); ?></td>
											</tr>
											<tr>
												<td><strong>Date Ordered</strong></td>
												<td><?php echo pretty_date($trans['trans_date']); ?></td>
											</tr>
											<?php if($trans['paid'] == 1) : ?>
											<tr>
												<td><strong>Date Completed</strong></td>
												<td><?php echo pretty_date($trans['completed_date']); ?></td>
											</tr>
											<?php endif; ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>

						<div class="col-md-12 tab-pane fade" id="shipping-address">
							<div class="panel panel-default">
								<div class="panel-heading text-center">Customer Details</div>
								<div class="panel-body">
									<table class="table">
										<tbody>
											<tr>
												<td><strong>Customer Name</strong></td>
												<td><?php echo $trans['full_name']; ?></td>
											</tr>
											<tr>
												<td><strong>Contact No.</strong></td>
												<td><?php echo $trans['phone']; ?></td>
											</tr>
											<tr>
												<td><strong>Email</strong></td>
												<td><?php echo $trans['email']; ?></td>
											</tr>
											<tr>
												<td><strong>Street Address</strong></td>
												<td><?php echo $trans['street'].(($trans['street2'] != '')?', '.$trans['street2']:''); ?></td>
											</tr>
											<tr>
												<td><strong>City & Zipcode</strong></td>
												<td><?php echo $trans['city'].' City, '.$trans['zipcode']; ?></td>
											</tr>
											<tr>
												<td><strong>Country</strong></td>
												<td><?php echo $trans['country']; ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default-grey" onclick="closeModal();">Close</button>
				<?php if($trans['paid'] == 0) : ?>
				<button type="button" class="btn btn-primary" onclick="complete_order(<?php echo $trans['id']; ?>)">Complete Order</button>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<script>
	function closeModal(){
		jQuery('#order_details').modal('hide');
		setTimeout(function(){
			jQuery('#order_details').remove();
			jQuery('.modal-backdrop').remove();
		});
	}
</script>


<?php echo ob_get_clean(); ?>