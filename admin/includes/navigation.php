
	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand" href="index.php">Administrator</a>
			</div>
			
			<div class="collapse navbar-collapse" id="navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="categories.php">Categories</a></li>
					<li class="dropdown">
						<!--<a href="products.php">Products</a>-->
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Products <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="products.php?add=1"><span class="glyphicon glyphicon-plus"></span> Add Product</a></li>
							<li><a href="products.php"><span class="glyphicon glyphicon-list"></span> Products</a></li>
							<li><a href="archived.php"><span class="glyphicon glyphicon-folder-close"></span> Archived</a></li>
						</ul>
					</li>
					<?php if(has_permission('admin')) : ?>
					<li class="dropdown">
						<!--<a href="users.php">Users</a>-->
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Users <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="users.php?add=1"><span class="glyphicon glyphicon-user"></span> Add User</a></li>
							<li><a href="users.php"><span class="glyphicon glyphicon-list"></span> Users</a></li>
							<li><a href="deactivated_users.php"><span class="glyphicon glyphicon-lock"></span> Deactivated</a></li>
						</ul>
					</li>
					<?php endif; ?>
					<li><a href="notes.php">Notes</a></li>
				</ul>
				
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hello, <?php echo $user_data['first']; ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<!--<li><a href="change_password.php">Change Password</a></li>-->
							<li><a href="account.php">My Account</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="logout.php">Log Out</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</nav>