<?php
	require_once '../core/init.php';
	if(!is_logged_in()){
		login_error_redirect();
	}
	if($user_data['permissions'] != 'admin,editor'){
		login_error_redirect();
	}
	if(!has_permission('admin')){
		permission_error_redirect('index.php');
	}
	include 'includes/head.php';
	include 'includes/navigation.php';
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="page-header">My Account</h2>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<label for="full_name">Full Name</label>
				<input type="text" class="form-control" id="full_name" name="full_name" value="<?php echo $user_data['full_name']; ?>" required>
			</div>

			<div class="form-group">
				<label for="email">Email</label>
				<input type="email" class="form-control" id="email" name="email" value="<?php echo $user_data['email'] ?>" required>
			</div>

			<div class="form-group">
				<button id="flip" type="button" class="btn btn-default"><span class="glyphicon glyphicon-menu-down"></span> More Details (Optional)</button>
				<a href="change_password.php" class="btn btn-default"><span class="glyphicon glyphicon-cog"></span> Change Password</a>
			</div>

			<div id="panel">
				<div class="form-group">
					<label for="phone">Phone</label>
					<input type="text" class="form-control" name="phone" id="phone" value="<?php echo $user_data['phone']; ?>">
				</div>

				<div class="form-group">
					<label for="street">Address</label>
					<input type="text" class="form-control" name="street" id="street" value="<?php echo $user_data['street']; ?>">
				</div>

				<div class="form-group">
					<label for="street2" class="sr-only">Address 2</label>
					<input type="text" class="form-control" name="street2" id="street2" placeholder="Aparment, suit, unit etc." value="<?php echo $user_data['street2']; ?>">
				</div>

				<div class="form-group">
					<label for="city">City</label>
					<input type="text" class="form-control" name="city" id="city" value="<?php echo $user_data['city']; ?>">
				</div>

				<div class="form-group">
					<label for="state">State/Province</label>
					<input type="text" class="form-control" name="state" id="state" value="<?php echo $user_data['state']; ?>">
				</div>

				<div class="form-group">
					<label for="country">Country</label>
					<input type="text" class="form-control" name="country" id="country" value="<?php $user_data['country'] ?>" readonly>
				</div>
			</div>

			<div class="form-group">
				<input type="submit" name="submit" value="Save Changes" class="btn btn-success">
			</div>
		</div>
	</div>
</div>

<script> 
	$(document).ready(function(){
	    $("#flip").click(function(){
	        $("#panel").slideToggle("slow");
	    });
	});
</script>

<?php include 'includes/footer.php'; ?>