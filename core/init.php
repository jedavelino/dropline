<?php
	$host = '127.0.0.1';$username = 'root';$password = '';$dbname = 'dropline';
	
	// Create connection
	$conn = mysqli_connect($host, $username, $password, $dbname);
	
	// Check connection
	if($conn->connect_error){
		die("Connection failed: " . $conn->connect_error);
	}

	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/config.php';
	require_once BASEURL.'helpers/helpers.php';

	$cartID = '';
	if(isset($_COOKIE[CART_COOKIE])){
		$cartID = sanitize($_COOKIE[CART_COOKIE]);
	}
	
	// get user data
	if(isset($_SESSION['User'])){
		$user_id = $_SESSION['User'];
		$user_query = $conn->query("SELECT * FROM users WHERE id = '{$user_id}'");
		$user_data = mysqli_fetch_assoc($user_query);
		// get the user full name
		$full_name = explode(' ', $user_data['full_name']);
		$user_data['first'] = $full_name[0];
		$user_data['last'] = $full_name[1];
	}
	
	// session flash checks
	if(isset($_SESSION['success_flash'])){
		echo '<div class="alert" role="alert" style="margin-top:-20px"><p class="text-success text-center">'.$_SESSION['success_flash'].'</p></div>';
		unset($_SESSION['success_flash']);
	}
	
	if(isset($_SESSION['error_flash'])){
		echo '<div class="alert" style="margin-top:-20px;"><p class="text-danger text-center">'.$_SESSION['error_flash'].'</p></div>';
		unset($_SESSION['error_flash']);
	}
?>