<?php
	require_once '../core/init.php';
	$pID = $_POST['id'];
	$pID = (int)$pID;
	$p_query = $conn->query("SELECT * FROM products WHERE id = '{$pID}'");
	$product = mysqli_fetch_assoc($p_query);
	$childID = $product['categories'];
	$category_result = $conn->query("SELECT * FROM categories WHERE id = '{$childID}'");
	$child = mysqli_fetch_assoc($category_result);
	$parentID = $child['parent'];
	$parent_result = $conn->query("SELECT * FROM categories WHERE id = '{$parentID}'");
	$parent = mysqli_fetch_assoc($parent_result);
	$category = $parent['category'].', '.$child['category'];
	$size_string = $product['sizes'];
	$size_array = explode(',',$size_string);
?>
<?php ob_start(); ?>
<div class="modal fade" id="details_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" type="button" onclick="closeModal()" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title text-center" id="myModalLabel"><?php echo $product['name']; ?></h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<span id="modal-error" class="bg-danger"></span>
					<div class="col-md-6 fotorama">
						<?php if($product['image'] != '') : ?>
						<?php $photos = explode(',',$product['image']); 
							foreach($photos as $photo) :
						?>
							<img src="<?php echo $photo; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive center-block"/>
						<?php endforeach; ?>
						<?php else : ?>
						<p>No Image Available.</p>
						<?php endif; ?>
					</div>
					<div class="col-md-6">
						<p><?php echo $product['description']; ?></p>
						<hr>
						<p><span>Categories:</span> <?php echo $category; ?></p>
						<p class="price"><?php echo money($product['price']); ?></p>
						<form action="addcart.php" method="post" id="add_product_to_cart">
							<input type="hidden" name="pid" value="<?php echo $pID; ?>">
							<div class="row">
								<div class="col-md-8">
									<div class="form-group">
										<label for="size">Size</label>
										<select class="form-control" name="size" id="size">
											<option value="">Select an option</option>
											<?php foreach($size_array as $string){
												$size = $string;
												echo '<option value="'.$size.'">'.$size.'</option>';
											}?>
										</select>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="quantity">Quantity</label>
										<input type="number" class="form-control" id="quantity" name="quantity" min="1" placeholder="1">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<input type="submit" onclick="add_to_cart();return false;" class="btn btn-primary btn-lg" value="Add To Cart">
									</div>
								</div>
							</div>
						</form>						
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default-grey" type="button" onclick="closeModal()">Close</button>
			</div>
		</div>
	</div>
</div>

<script>
	$(function () {
	  $('.fotorama').fotorama({
	  	'loop' : true,
	  	'autoplay' : true,
	  	'transition' : 'crossfade',
	  	'shadows' : true,
	  	'nav' : 'thumbs',
	  	'thumbwidth' : '48px',
	  	'thumbheight' : '48px',
	  	'thumbmargin' : 10,
	  });
	});

	function closeModal() {
		jQuery('#details_modal').modal('hide');
		setTimeout(function() {
			jQuery('#details_modal').remove();
			jQuery('.modal-backdrop').remove();
		},500);
	}
</script>

<?php echo ob_get_clean(); ?>