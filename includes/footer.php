	<!-- Footer -->

<footer class="footer">
	<div class="container">
	<!--<hr>-->
		<div class="row">

			<div class="col-sm-4">
				<h5>About Us</h5>
				<p>Dropline Co. is a type of business that manufactures t-shirts with minimalist prints. It is also an E-Commerce website or online store where customers can browse, buy and be entertained with the products displayed in catalogue form.</p>
			</div>

			<div class="col-sm-2">
				<h5>Navigation</h5>
				<ul class="list-unstyled">
					<li><a href="#">Home</a></li>
					<li><a href="#">Products</a></li>
					<li><a href="#">Links</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>

			<div class="col-sm-2">
				<h5>Follow Us</h5>
				<ul class="list-unstyled">
					<li><a href="#">Facebook</a></li>
					<li><a href="#">Twitter</a></li>
					<li><a href="#">Instagram</a></li>
				</ul>				
			</div>

			<div class="col-sm-4">
				<h5>Copyright &copy; 2015 Dropline Co. Made with love.</h5>
			</div>
		</div>
	</div>
</footer>

<script>

	function details_modal(id){
		var data = {"id" : id};
		jQuery.ajax({
			url : '/dropline/includes/details_modal.php',
			method : 'post',
			data : data,
			success : function(data){
				jQuery('body').append(data);
				jQuery('#details_modal').modal('toggle');
			},
			error : function(){
				alert("Something went wrong.");
			}
		});
	}

	function update_cart(mode, eid, esize){
		var data = {
			"mode" : mode,
			"eid" : eid,
			"esize" : esize
		};
		jQuery.ajax({
			url : '/dropline/admin/parsers/updcart.php',
			method : "post",
			data : data,
			success : function(){
				location.reload();
			},
			error : function(){
				alert("Something went wrong!");
			}
		});
	}

	function add_to_cart(){
		jQuery('#modal-error').html("");
		var size = jQuery('#size').val();
		var quantity = jQuery('#quantity').val();
		var error = '';
		var data = jQuery('#add_product_to_cart').serialize();
		if(size == '' || quantity == '' || quantity == 0){
			error += '<p class="text-danger text-center">You must fill out size and quantity.</p>';
			jQuery('#modal-error').html(error);
			return;
		} else {
			jQuery.ajax({
				url : '/dropline/admin/parsers/addcart.php',
				method : 'post',
				data : data,
				success : function(){
					location.reload();
					//window.location.href = '/dropline/cart.php';
				},
				error : function(){
					alert("Something went wrong!");
				}
			});
		}
	}

	function login(){
		jQuery('#login-error').html("");
		var email = jQuery('#email').val();
		var password = jQuery('#password').val();
		var error = '';
		var data = jQuery('#login-user').serialize();
		if(email == '' || password == ''){
			error += 'You must enter email and password.';
			jQuery('#modal-error').html(error);
			return;
		} else {
			jQuery.ajax({
				url : '/dropline/customer/parsers/login.php',
				method : 'post',
				data : data,
				success : function(){
					location.reload();
				},
				error : function(){
					alert("Something went wrong!");
				}
			});
		}
	}

	/*function submit_forms(){
			var cart_id = jQuery('#cart_id').val();
			var subtotal = jQuery('#subtotal').val();
			var ordertotal = jQuery('#ordertotal').val();
			var description = jQuery('#description').val();
			var full_name = jQuery('#full_name').val();
			var email = jQuery('#email').val();
			var phone = jQuery('#phone').val();
			var password = jQuery('#password').val();
			var confirm_password = jQuery('#confirm_password').val();
			var street = jQuery('#street').val();
			var street2 = jQuery('#street2').val();
			var city = jQuery('#city').val();
			var state = jQuery('#state').val();
			var zipcode = jQuery('#zipcode').val();
			var country = jQuery('#country').val();
			var data = jQuery('#form2').serialize();
			jQuery.ajax({
				url : '/dropline/cart.php',
				method : 'post',
				data : data,
				success : function(){
					jQuery('#form1').submit();
					window.location.href='order.php';
				},
				error : function(){alert("Something went wrong");}
			});	
			return false; 
	}*/
</script>
	

</body>


</html>