<?php
	$parent_result = $conn->query("SELECT * FROM categories WHERE parent = 0");
	
	$subtotal = 0;
	$item_count = 0;
	
	if($cartID != ''){
		$cartQ = $conn->query("SELECT * FROM cart WHERE id = '{$cartID}'");
		$result = mysqli_fetch_assoc($cartQ);
		$items = json_decode($result['items'], true);
		$subtotal = 0;
		$item_count = 0;

		foreach($items as $item){
			$pID = $item['id'];
			$productQ = $conn->query("SELECT * FROM products WHERE id = '{$pID}'");
			$product = mysqli_fetch_assoc($productQ);
			$item_count += $item['quantity'];
			$subtotal += ($product['price'] * $item['quantity']);
		}
	}

?>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="index.php">Dropline Co.</a>
		</div>
		
		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<?php while($parent = mysqli_fetch_assoc($parent_result)) : ?>
				<?php
					$parentID = (int)$parent['id'];
					$child_result = $conn->query("SELECT * FROM categories WHERE parent = '$parentID'");
				?>
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $parent['category']; ?><span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<?php while($child = mysqli_fetch_assoc($child_result)) : ?>
						<li>
							<a href="category.php?category=<?php echo $child['id']; ?>"><?php echo $child['category']; ?></a>
						</li>
						<?php endwhile; ?>
					</ul>
				</li>
				<?php endwhile; ?>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/dropline/customer/">Sign In</a></li>
				<li><a href="cart.php"><strong>Cart <?php echo (($subtotal != '' || $subtotal != 0)?'/ '.money($subtotal):''); ?></strong>&nbsp;&nbsp;<span class="label label-<?php echo (($item_count != 0)?'danger':'default'); ?>"><?php echo (($item_count != 0)?$item_count:'0'); ?></span></a></li>
			</ul>
		</div>
	</div>
</nav>