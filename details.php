<?php
	require_once 'core/init.php';
	include 'includes/head.php';
	include 'includes/navigation.php';
	error_reporting(0);
	if(isset($_GET['id'])){
		$pID = (int)$_GET['id'];
		$result = $conn->query("SELECT * FROM products WHERE id = '{$pID}'");
		$product = mysqli_fetch_assoc($result);
		$childID = $product['categories'];
		$category_result = $conn->query("SELECT * FROM categories WHERE id = '{$childID}'");
		$child = mysqli_fetch_assoc($category_result);
		$parentID = $child['parent'];
		$parent_result = $conn->query("SELECT * FROM categories WHERE id = '{$parentID}'");
		$parent = mysqli_fetch_assoc($parent_result);
		$category = $parent['category'].', '.$child['category'];
		$size_string = $product['sizes'];
		$size_array = explode(',',$size_string);
	}
?>

<div class="container" style="margin-top:50px">
	<div class="row">
		<div class="col-md-12">
			<span id="modal-error" class="bg-danger"></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-2">
			<div class="fotorama">
				<?php if($product['image'] != '') : ?>
					<?php $photos = explode(',',$product['image']); ?>
						 <?php foreach($photos as $photo) : ?>
							<img src="<?php echo $photo; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive center-block"/>
						<?php endforeach; ?>
				<?php else : ?>
				<p>No Image Available.</p>
				<?php endif; ?>
			</div>
		</div>

		<div class="col-md-4">
			<h2><?php echo $product['name']; ?></h2>
			<p><?php echo $product['description']; ?></p>
			<hr>
			<p><span>Categories:</span> <?php echo $category; ?></p>
			<p class="price"><?php echo money($product['price']); ?></p>
			<form action="addcart.php" method="post" id="add_product_to_cart">
				<input type="hidden" name="pid" value="<?php echo $pID; ?>">
				<div class="row">
					<div class="col-md-8">
						<div class="form-group">
							<label for="size">Size</label>
							<select class="form-control" name="size" id="size">
								<option value="">Select an option</option>
								<?php foreach($size_array as $string){
									$size = $string;
									echo '<option value="'.$size.'">'.$size.'</option>';
								}?>
							</select>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label for="quantity">Quantity</label>
							<input type="number" class="form-control" id="quantity" name="quantity" min="1" placeholder="1">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input type="submit" onclick="add_to_cart();return false;" class="btn btn-primary btn-lg" value="Add To Cart">
						</div>
					</div>
				</div>
			</form>
		</div>	
	</div>
</div>

<script>
	$(function () {
	  $('.fotorama').fotorama({
	  	'loop' : true,
	  	'autoplay' : true,
	  	'transition' : 'crossfade',
	  	'shadows' : true,
	  	'nav' : 'thumbs',
	  	'thumbwidth' : '48px',
	  	'thumbheight' : '48px',
	  	'thumbmargin' : 10,
	  });
	});
</script>


<?php include 'includes/footer.php';