<?php
	require_once 'core/init.php';
	include 'includes/head.php';
	include 'includes/navigation.php';
	error_reporting(0);
	if($cartID != ''){
		$trans_query = $conn->query("SELECT * FROM transactions WHERE cart_id = '{$cartID}'");
		$transaction = mysqli_fetch_assoc($trans_query);
		$domain = ($_SERVER['HTTP_HOST'] != 'localhost')?'.'.$_SERVER['HTTP_HOST']:false;
		setcookie(CART_COOKIE,'',1,"/",$domain,false);
	}

?>
<div class="container">
	<div class="row">
	<h2 class="text-center">Thank you for purchasing.</h2>
		<div class="col-md-6 col-md-offset-3" style="margin-top:20px;">
			
			<p>Your order number is <?php echo $transaction['id']; ?>. 
			It usually takes 4-7 business days to deliver your items into your home or address. 
			Your shipping address are as follows:</p>

			<address>
				<strong><?php echo $transaction['full_name']; ?></strong><br>
				<a href="mailto:<?php echo $transaction['email']; ?>"><?php echo $transaction['email']; ?></a><br>
				<abbr title="Phone">P:</abbr> <?php echo $transaction['phone']; ?>
			</address>
			
			<address>
				<strong><?php echo $transaction['street']; ?><?php echo (($transaction['street2'] != '')?', '.$transaction['street2']:''); ?></strong><br>
				<?php echo $transaction['city']; ?>, <?php echo $transaction['state']; ?>, <?php echo $transaction['zipcode']; ?><br>
				<?php echo $transaction['country']; ?><br>
			</address>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			If you have some concerns or query in regards with your order, you can email us at <a href="mailto:dropline@gmail.com">dropline@gmail.com</a>. Thank you!
		</div>
		<div class="col-md-12 text-center">
			<a href="index.php" class="btn btn-default" style="margin-top:10px;">Return to shop</a>
			<a href="customer/index.php" class="btn btn-default" style="margin-top:10px;">Go to Account</a>
		</div>
	</div>
</div>

<?php include 'includes/footer.php'; ?>