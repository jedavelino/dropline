<?php
	require_once 'core/init.php';
	include 'includes/head.php';
	include 'includes/navigation.php';
	if(isset($_GET['category'])){
		$category_id = sanitize($_GET['category']);
	} else {
		$category_id = '';
	}
	$product_category_result = $conn->query("SELECT * FROM products WHERE categories = '$category_id' AND archived = 0");
	$category = get_category($category_id);
?>
	
<!-- Content -->
<div class="container">
	<h3 class="text-center"><?php echo $category['parent'].'\'s '.$category['child']; ?></h3>
	<hr>
	<?php while($product = mysqli_fetch_assoc($product_category_result)) : ?>
	<div class="col-md-3 col-sm-4 col-xs-6">
		<div class="thumbnail">
			<?php $photos = explode(',',$product['image']); ?>
			<a href="details.php?id=<?php echo $product['id']; ?>">
				<div>
					<img class="img-responsive" src="<?php echo $photos[0]; ?>" alt="<?php echo $product['name']; ?>">
				</div>
			</a>
			<div style="width:10%;height:1px;margin:0 auto;background:#333;"></div>
			<div class="caption cap">
				<h4 class="text-center product-name"><?php echo $product['name']; ?></h4>
				<p class="text-center price"><?php echo money($product['price']); ?></p>
				<button class="btn btn-default quick-view-btn btn-block" onclick="details_modal(<?php echo $product['id']; ?>);" type="button">
					Quick View
				</button>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
</div>
	
<?php include 'includes/footer.php'; ?>