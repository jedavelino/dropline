<?php
	require_once '../core/init.php';
	if(!is_logged_in()){
		login_error_redirect();
	}
	if($user_data['permissions'] != 'customer'){
		login_error_redirect();
	}
	include 'includes/head.php';
	include 'includes/navigation.php';
	$hashed_password_from_db = $user_data['password']; // gets the hashed password in the database. the associative array '$user_data' is initialized in init.php
	if(isset($_POST['old_password'])){$old_password = sanitize($_POST['old_password']);$old_password = trim($old_password);}else{$old_password = '';}
	if(isset($_POST['password'])){$password = sanitize($_POST['password']);$password = trim($password);}else{$password = '';}
	if(isset($_POST['confirm_new_password'])){$confirm_new_password = sanitize($_POST['confirm_new_password']);$confirm_new_password = trim($confirm_new_password);}else{$confirm_new_password = '';}
	$new_hashed_password = password_hash($password, PASSWORD_DEFAULT);
	$user_id = $user_data['id'];
	$errors = array();
?>
<h2 class="text-center">Change Password</h2>
<hr>
<div class="container">
	<div>
		<?php
			if($_POST){
				
				// password is more than 6 char
				if(strlen($password) < 6){
					$errors[] = 'Password must be at least 6 characters.';
				}
				
				// check if new password matches confirm new password
				if($password != $confirm_new_password){
					$errors[] = 'Your new passwords do not match.';
				}
				
				if(!password_verify($old_password, $hashed_password_from_db)){
					$errors[] = 'Your old password is invalid.';
				}
				
				if(!empty($errors)){
					echo display_errors($errors);
				} else {
					// change password
					$conn->query("UPDATE users SET password = '$new_hashed_password' WHERE id = '$user_id'");
					$_SESSION['success_flash'] = 'Your password has been updated.';
					header('Location: index.php');
				}
			}
		?>
	</div>	
	<form class="form-ch-pass" action="change_password.php" method="post">
		<div class="form-group">
			<label for="old_password">Old Password</label>
			<input type="password" name="old_password" id="old_password" value="<?php echo $old_password; ?>" class="form-control" required>
		</div>
		
		<div class="form-group">
			<label for="password">New Password</label>
			<input type="password" name="password" id="password" value="<?php echo $password; ?>" class="form-control" required>
		</div>
		
		<div class="form-group">
			<label for="confirm_new_password">Confirm New Password</label>
			<input type="password" name="confirm_new_password" id="confirm_new_password" value="<?php echo $confirm_new_password; ?>" class="form-control" required>
		</div>
		
		<div class="form-group">
			<a href="index.php" class="btn btn-default">Cancel</a>
			<input type="submit" name="submit" value="Save Changes" class="btn btn-success">
		</div>
	</form>

</div>




<?php include 'includes/footer.php'; ?>