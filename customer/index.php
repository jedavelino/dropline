<?php
	require_once '../core/init.php';
	if(!is_logged_in()){
		header('Location: login.php');
	}
	if($user_data['permissions'] != 'customer'){
		//login_error_redirect();
		header('Location: login.php');
	}

	include 'includes/head.php';
	include 'includes/navigation.php';

	$userID = $user_data['id'];
	$userEmail = (($user_data['email'] != '')?$user_data['email']:''); /*$user_data['email'];*/

	$transq = "SELECT t.id, t.cart_id, t.full_name, t.trans_date, t.ordertotal FROM transactions t LEFT JOIN cart c ON t.cart_id = c.id WHERE paid = 0 AND email = '{$userEmail}' ORDER BY t.trans_date";
	$result = $conn->query($transq);

	$comp_transq = "SELECT t.id, t.cart_id, t.full_name, t.trans_date, t.ordertotal, t.completed_date, t.paid FROM transactions t LEFT JOIN cart c ON t.cart_id = c.id WHERE paid = 1 AND email = '{$userEmail}' ORDER BY t.completed_date";
	$comp_trans_result = $conn->query($comp_transq);
	//$comp_trans_order = mysqli_fetch_assoc($comp_trans_result);

	$comp_trans_paid0 = $conn->query("SELECT * FROM transactions WHERE paid = 0 AND email = '{$userEmail}'");
	$comp_trans_order_paid0 = mysqli_fetch_assoc($comp_trans_paid0);

	$comp_trans_paid1 = $conn->query("SELECT * FROM transactions WHERE paid = 1 AND email = '{$userEmail}'");
	$comp_trans_order_paid1 = mysqli_fetch_assoc($comp_trans_paid1);
?>
<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h4>Your Pending Orders</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<?php if($comp_trans_order_paid0['paid'] == 1) : ?>
							<p class="text-center">You currently have no pending orders.</p>
							<?php else : ?>
							<div class="table-responsive">
								<table class="table table-condensed">
									<thead>
										<th>Order No.</th><th>Ordered By</th><th>Total</th><th>Date Ordered</th><th></th>
									</thead>
									<tbody>
									<?php while($order = mysqli_fetch_assoc($result)) : ?>
										<tr>
											<td class="text-center"><?php echo $order['id']; ?></td>
											<td><?php echo $order['full_name']; ?></td>
											<td><?php echo money($order['ordertotal']); ?></td>
											<td><?php echo pretty_date2($order['trans_date']); ?></td>
											<td><button type="button" class="btn btn-xs btn-default-grey" onclick="order_details(<?php echo $order['id']; ?>);">Details</button></td>
										</tr>
									<?php endwhile; ?>
									</tbody>
								</table>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading text-center">
					<h4>Your Completed Orders</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<?php if($comp_trans_order_paid1['paid'] == 0) : ?>
							<p class="text-center">You currently have no completed orders yet.</p>
							<?php else : ?>
							<div class="table-responsive">
								<table class="table table-condensed">
									<thead>
										<th>Order No.</th><th>Delivered To</th><th>Total</th><th>Date Completed</th><th></th>
									</thead>
									<tbody>
									<?php while($order = mysqli_fetch_assoc($comp_trans_result)) : ?>
										<tr>
											<td class="text-center"><?php echo $order['id']; ?></td>
											<td><?php echo $order['full_name']; ?></td>
											<td><?php echo money($order['ordertotal']); ?></td>
											<td><?php echo pretty_date2($order['completed_date']); ?></td>
											<td><button type="button" class="btn btn-xs btn-default-grey" onclick="order_details(<?php echo $order['id']; ?>);">Details</button></td>
										</tr>
									<?php endwhile; ?>
									</tbody>
								
								</table>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<a href="../cart.php?checkout=1&user_id=<?php echo $userID; ?>" class="btn btn-default">Continue Shopping...</a>
		</div>
	</div>
</div>

<?php include 'includes/footer.php'; ?>