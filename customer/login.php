<?php
	require_once $_SERVER['DOCUMENT_ROOT'].'/dropline/core/init.php';
	include 'includes/head.php';

	if(isset($_POST['email'])){$email = sanitize($_POST['email']);$email = trim($email);}else{$email = '';}
	if(isset($_POST['password'])){$password = sanitize($_POST['password']);$password = trim($password);}else{$password = '';}
	$errors = array();
?>

<style>
	body{padding-top:20;}
</style>

	<div class="container">
		<h3 class="text-center">Customer</h3>
		<div>
			<?php
				if($_POST){
					
					// validate email
					if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
						$errors[] = 'The email you entered is invalid.';
					}
					
					// password is more than 6 char
					if(strlen($password) < 6){
						$errors[] = 'Your password must be at least 6 characters.';
					}
					
					// checking if email already exist
					$user_query = $conn->query("SELECT * FROM users WHERE email = '$email'");
					$user = mysqli_fetch_assoc($user_query);
					$user_count = mysqli_num_rows($user_query);
					if($user_count < 1){
						$errors[] = 'The email you entered already exist.';
					}
					
					// check if the account has been deactivated
					if($user['deactivated'] == 1) {
						$errors[] = 'Your account has been deactivated.';
					}
					
					if(!password_verify($password, $user['password'])){
						$errors[] = 'The password you entered is invalid.';
					}

					$user_type = $user['permissions'];
					
					if(!empty($errors)){
						echo display_errors($errors);
					} else {
						// log user in
						$user_id = $user['id'];
						if($user_type == 'customer'){
							login_cust($user_id);
						} else {
							echo '<p class="text-center text-danger">You are not authorized to login into this page.</p>';
						}
						
						//echo $user_type;*/
					}
				}
			?>
		</div>
		<form action="login.php" method="post" class="form-login">
			<label for="email" class="sr-only">Email</label>
			<input type="email" name="email" id="email" class="form-control" value="<?php echo $email; ?>" placeholder="Email" required>

			<label for="password" class="sr-only">Password</label>
			<input type="password" name="password" id="password" class="form-control" value="<?php echo $password; ?>" placeholder="Password" required>

			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign In</button>
		</form>
		<p class="text-center"><a href="/dropline/index.php" alt="home">Back to Homepage</a></p>
	</div>