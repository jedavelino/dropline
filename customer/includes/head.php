<!DOCTYPE html>
<html lang="en">
<head>
	<!-- Important meta tags for mobile devices -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- Title -->
	<title>Customer</title>
	
	<!-- Bootstrap wrap with bootswatch -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	
	<!-- Custom CSS -->
	<link rel="stylesheet" href="css/main.css">
	
	<!-- Jquery script for Bootstrap -->
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<!-- Bootstrap javascript -->
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
</head>

<body>