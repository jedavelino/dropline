<!-- Footer -->
<br><br>
<footer class="footer">
	<div class="container">
		<h5 class="text-center">Copyright &copy; 2015 Dropline Co.</h5>
	</div>
</footer>

<script>
	function order_details(id){
		var data = {"id" : id};
		jQuery.ajax({
			url : '/dropline/customer/includes/order_details.php',
			method : "post",
			data : data,
			success : function(data){
				jQuery('body').append(data);
				jQuery('#order_details').modal('toggle');
			},
			error : function(){
				alert("Something went wrong.");
			}
		});
	}
</script>

</body>


</html>