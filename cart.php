<?php
	require_once 'core/init.php';
	include 'includes/head.php';
	include 'includes/navigation.php';
	error_reporting(0);
	if($cartID != ''){
		$cartQ = $conn->query("SELECT * FROM cart WHERE id = '{$cartID}'");
		$result = mysqli_fetch_assoc($cartQ);
		$items = json_decode($result['items'], true);
		$subtotal = 0;
		$item_count = 0;	
	}

	if(isset($_GET['checkout'])){
		$cart_id = ((isset($_POST['cart_id']) && $_POST['cart_id'] != '')?sanitize($_POST['cart_id']):'');
		$subtotal = ((isset($_POST['subtotal']) && $_POST['subtotal'] != '')?sanitize($_POST['subtotal']):'');
		$ordertotal = ((isset($_POST['ordertotal']) && $_POST['ordertotal'] != '')?sanitize($_POST['ordertotal']):'');
		$description = ((isset($_POST['description']) && $_POST['description'] != '')?sanitize($_POST['description']):'');
		$full_name = ((isset($_POST['full_name']) && $_POST['full_name'] != '')?sanitize($_POST['full_name']):'');
		$email = ((isset($_POST['email']) && $_POST['email'] != '')?sanitize($_POST['email']):'');
		$phone = ((isset($_POST['phone']) && $_POST['phone'] != '')?sanitize($_POST['phone']):'');
		$password = ((isset($_POST['password']) && $_POST['password'] != '')?sanitize($_POST['password']):'');
		$confirm_password = ((isset($_POST['confirm_password']) && $_POST['confirm_password'] != '')?sanitize($_POST['confirm_password']):'');
		$street = ((isset($_POST['street']) && $_POST['street'] != '')?sanitize($_POST['street']):'');
		$street2 = ((isset($_POST['street2']) && $_POST['street2'] != '')?sanitize($_POST['street2']):'');
		$city = ((isset($_POST['city']) && $_POST['city'] != '')?sanitize($_POST['city']):'');
		$zipcode = ((isset($_POST['zipcode']) && $_POST['zipcode'] != '')?sanitize($_POST['zipcode']):'');
		$state = ((isset($_POST['state']) && $_POST['state'] != '')?sanitize($_POST['state']):'');
		$country = ((isset($_POST['country']) && $_POST['country'] != '')?sanitize($_POST['country']):'');

		if(isset($_GET['user_id']) && $cartID == ''){
			header("Location: index.php");
		} else {
			$userID = (int)$_GET['user_id'];
			$usersq_result = $conn->query("SELECT * FROM users WHERE id = '{$userID}'");
			$user = mysqli_fetch_assoc($usersq_result);
			$full_name = ((isset($_POST['full_name']) && $_POST['full_name'] != '')?sanitize($_POST['full_name']):$user['full_name']);
			$email = ((isset($_POST['email']) && $_POST['email'] != '')?sanitize($_POST['email']):$user['email']);
			$phone = ((isset($_POST['phone']) && $_POST['phone'] != '')?sanitize($_POST['phone']):$user['phone']);
			$street = ((isset($_POST['street']) && $_POST['street'] != '')?sanitize($_POST['street']):$user['street']);
			$street2 = ((isset($_POST['street2']) && $_POST['street2'] != '')?sanitize($_POST['street2']):$user['street2']);
			$city = ((isset($_POST['city']) && $_POST['city'] != '')?sanitize($_POST['city']):$user['city']);
			$zipcode = ((isset($_POST['zipcode']) && $_POST['zipcode'] != '')?sanitize($_POST['zipcode']):$user['zipcode']);
			$state = ((isset($_POST['state']) && $_POST['state'] != '')?sanitize($_POST['state']):$user['state']);
			$country = ((isset($_POST['country']) && $_POST['country'] != '')?sanitize($_POST['country']):$user['country']);
		}

		if($_POST['submit_new']){
			$errors = array();
			
			// checking if email already exist
			$user_query = $conn->query("SELECT * FROM users WHERE email = '$email'");
			$user = mysqli_fetch_assoc($user_query);
			$user_count = mysqli_num_rows($user_query);
			if($user_count != 0){
				$errors[] = 'That email already exist.';
			}
			
			if(strlen($password) < 6){
				$errors[] = 'Passwords must be at least 6 characters.';
			}
			
			if($password != $confirm_password){
				$errors[] = 'Passwords do not match.';
			}
			
			if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
				$errors[] = 'Enter a valid email.';
			}
			
			if(!empty($errors)){
				echo display_errors($errors);
			} else {
				// add user
				$hashed_password = password_hash($password, PASSWORD_DEFAULT);
				// Insert into users
				$conn->query("INSERT INTO users (full_name, email, password, phone, street, street2, city, state, zipcode, country, permissions) 
				VALUES ('{$full_name}', '{$email}', '{$hashed_password}', '{$phone}', '{$street}', '{$street2}', '{$city}', '{$state}', '{$zipcode}', '{$country}', 'customer')");

				// Insert into transactions
				$conn->query("INSERT INTO transactions (full_name, cart_id, email, phone, street, street2, city, state, zipcode, country, subtotal, ordertotal, description)
				VALUES ('{$full_name}', '{$cart_id}', '{$email}', '{$phone}', '{$street}', '{$street2}', '{$city}', '{$state}', '{$zipcode}', '{$country}', '{$subtotal}', '{$ordertotal}', '{$description}')");
				//$domain = ($_SERVER['HTTP_HOST'] != 'localhost')?'.'.$_SERVER['HTTP_HOST']:false;
				//setcookie(CART_COOKIE,'',1,"/",$domain,false);
				header('Location: order.php');
			}

		}//post

		if($_POST['submit_logged']){
			
			$conn->query("INSERT INTO transactions (full_name, cart_id, email, phone, street, street2, city, state, zipcode, country, subtotal, ordertotal, description)
			VALUES ('{$full_name}', '{$cart_id}', '{$email}', '{$phone}', '{$street}', '{$street2}', '{$city}', '{$state}', '{$zipcode}', '{$country}', '{$subtotal}', '{$ordertotal}', '{$description}')");
			header('Location: order.php');			
		}
?>

<div class="container">
<h2 class="text-center page-header">Checkout Details</h2>
<?php
	foreach($items as $item){
		$pID = $item['id'];
		$item_count += $item['quantity'];
		$subtotal += ($product['price'] * $item['quantity']);
	}
	$ordertotal = $subtotal;
?>
	<div class="row">
		<!-- Shipping details -->
		<div class="col-md-6">
		<h3>Shipping Details</h3>
		<?php if(!isset($_GET['user_id'])) : ?>
		<p>Returning customer? <a href="customer/">Click here...</a></p>
		<?php endif; ?>
		<!--<p>Returning customer? <button type="button" class="btn btn-link" data-toggle="modal" data-target="#loginModal" style="margin:0;padding:0;">Click here to login</button></p>

		<!-- Modal --
		<div class="modal fade login-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title text-center" id="loginModalLabel">Sign In</h4>
					</div>
					<div class="modal-body">
						<div class="row">
						<span id="login-error" class="text-center text-danger"></span>
							<div class="col-md-12">
								<form action="login.php" method="post" id="login-user">
									<div class="form-group">
										<label for="email">Email</label>
										<input checked="form-control" type="email" name="email" id="email" class="form-control" value="" placeholder="Email" required>
									</div>
									<div class="form-group">
										<label for="password">Password</label>
										<input class="form-control" type="password" name="password" id="password" class="form-control" value="" placeholder="Password" required>
									</div>
									<div class="form-group">
										<button type="button" onclick="login();" class="btn btn-primary btn-lg btn-block">Sign In</button>
									</div>					
								</form>
							</div>
						</div>
					</div>
					<!--<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" class="btn btn-primary">Save changes</button>
					</div>
					
				</div>
			</div>
		</div>-->

			<form action="order.php" method="post" id="form1">
				<input type="hidden" name="cart_id" value="<?php echo $cartID; ?>">
			</form>
			<form action="cart.php?checkout=1<?php echo ((isset($_GET['user_id']))?'&user_id='.$userID:''); ?>" method="post" id="form2">
				<input type="hidden" name="cart_id" id="cart_id" value="<?php echo $cartID; ?>">
				<input type="hidden" name="subtotal" id="subtotal" value="<?php echo $subtotal; ?>">
				<input type="hidden" name="ordertotal" id="ordertotal" value="<?php echo $ordertotal; ?>">
				<input type="hidden" name="description" id="description" value="<?php echo $item_count.' item'.(($item_count>1)?'s':'').' purchased.'; ?>">
				<div class="form-group <?php echo ((isset($_GET['user_id']))?'has-success has-feedback':''); ?>">
					<label for="full_name">Name</label>
					<input type="text" name="full_name" id="full_name" class="form-control" value="<?php echo $full_name; ?>" placeholder="Full Name" required <?php echo ((isset($_GET['user_id']))?'readonly':'') ?>>
					<?php if(isset($_GET['user_id'])) : ?>
					<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
					<?php endif; ?>
				</div>
				<div class="row">
					<div class="form-group col-md-6 <?php echo ((isset($_GET['user_id']))?'has-success has-feedback':''); ?>">
						<label for="email">Email</label>
						<input type="email" name="email" id="email" class="form-control" value="<?php echo $email; ?>" placeholder="Email Address" required <?php echo ((isset($_GET['user_id']))?'readonly':'') ?>>
					</div>
					<div class="form-group col-md-6 <?php echo ((isset($_GET['user_id']))?'has-success has-feedback':''); ?>">
						<label for="phone">Phone</label>
						<input type="text" name="phone" id="phone" class="form-control" value="<?php echo $phone; ?>" placeholder="Contact No." required <?php echo ((isset($_GET['user_id']))?'readonly':'') ?>>
					</div>
				</div>
				<?php if(!isset($_GET['user_id'])) : ?>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
				</div>
				<div class="form-group">
					<label for="confirm_password" class="sr-only">Confirm Password</label>
					<input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Password" required>
				</div>
				<?php endif; ?>
				<div class="form-group <?php echo ((isset($_GET['user_id']))?'has-success has-feedback':''); ?>">
					<label for="street">Street Address</label>
					<input type="text" name="street" id="street" class="form-control" value="<?php echo $street; ?>" placeholder="Street Address" required <?php echo ((isset($_GET['user_id']))?'readonly':'') ?>>
					<?php if(isset($_GET['user_id'])) : ?>
					<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
					<?php endif; ?>
				</div>
				<div class="form-group <?php echo ((isset($_GET['user_id']))?'has-success has-feedback':''); ?>">
					<label for="street2" class="sr-only">Address 2</label>
					<input type="text" name="street2" id="street2" class="form-control" value="<?php echo $street2; ?>" placeholder="Aparment, suit, unit etc. (Optional)" <?php echo ((isset($_GET['user_id']))?'readonly':'') ?>>
					<?php if(isset($_GET['user_id'])) : ?>
					<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
					<?php endif; ?>				
				</div>
				<div class="row">
					<div class="form-group col-md-6 <?php echo ((isset($_GET['user_id']))?'has-success has-feedback':''); ?>">
						<label for="city">City</label>
						<input type="text" name="city" id="city" class="form-control" value="<?php echo $city; ?>" placeholder="City" required <?php echo ((isset($_GET['user_id']))?'readonly':'') ?>>				
					</div>
					<div class="form-group col-md-6 <?php echo ((isset($_GET['user_id']))?'has-success has-feedback':''); ?>">
						<label for="zipcode">Zip Code</label>
						<input type="text" name="zipcode" id="zipcode" class="form-control" value="<?php echo $zipcode; ?>" placeholder="Zip Code" required <?php echo ((isset($_GET['user_id']))?'readonly':'') ?>>					
					</div>
				</div>
				<div class="form-group <?php echo ((isset($_GET['user_id']))?'has-success has-feedback':''); ?>">
					<label for="state">State/Province</label>
					<input type="text" name="state" id="state" class="form-control" value="<?php echo $state; ?>" placeholder="State/Province" required <?php echo ((isset($_GET['user_id']))?'readonly':'') ?>>
					<?php if(isset($_GET['user_id'])) : ?>
					<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
					<?php endif; ?>				
				</div>
				<div class="form-group <?php echo ((isset($_GET['user_id']))?'has-success has-feedback':''); ?>">
					<label for="country">Country</label>
					<input type="text" name="country" id="country" class="form-control" value="<?php echo ((!isset($_GET['user_id']))?'Philippines':$country); ?>" required <?php echo ((isset($_GET['user_id']))?'readonly':'readonly') ?>>
					<?php if(isset($_GET['user_id'])) : ?>
					<span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
					<?php endif; ?>				
				</div>
				<!--<div class="form-group">
					<input type="submit" name="submit" class="btn btn-lg btn-default" value="Place Order">
				</div>
				<!--<button type="button" class="btn btn-lg btn-default" onclick="submitForms();">Place Order</button>--
			</form>
				<!--<button type="button" class="btn btn-lg btn-default" onclick="submit_forms();">Place Order</button>-->
		</div>
		<!-- End of shipping details -->

		<!-- Order -->
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading text-center"><h3>Your Order</h3></div>
				<div class="panel-body">
					<table class="table table-striped">
						<thead>
							<th>Product</th><th>Total</th>
						</thead>
						<tbody>
						<?php
						$item_count = '';
						$subtotal = '';
						$ordertotal = '';
						foreach($items as $item){
							$pID = $item['id'];
							$productQ = $conn->query("SELECT * FROM products WHERE id = '{$pID}'");
							$product = mysqli_fetch_assoc($productQ);
						?>
							<tr>
								<td>
									<?php echo $product['name']; ?><br>
									<small class="text-muted">Size: <?php echo $item['size']; ?> x <?php echo $item['quantity']; ?></small>
								</td>
								<td><?php echo money($product['price']); ?></td>
							</tr>
							<?php 
								$item_count += $item['quantity'];
								$subtotal += ($product['price'] * $item['quantity']);
							} 
								$ordertotal = $subtotal;
							?>
						</tbody>
					</table>
					<table class="table">
						<tbody>
							<tr>
								<td><strong>Cart Subtotal</strong></td>
								<td><?php echo money($subtotal); ?></td>
							</tr>
							<tr>
								<td><strong>Order Total</strong></td>
								<td><?php echo money($ordertotal); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<!-- Place order button -->
			<div class="form-group">
				<?php if(!isset($_GET['user_id'])) : ?>
				<input type="submit" name="submit_new" class="btn btn-lg btn-default" value="Place Order">
				<?php else : ?>
				<input type="submit" name="submit_logged" class="btn btn-lg btn-default" value="Place Order">
				<?php endif; ?>
			</div>
			</form>
		</div>
		<!-- end of Order -->
	</div>
</div>

<?php } else { ?>

<div class="container">
	<h2>My Shopping Cart</h2>
	<div class="row">
		<?php if($cartID == '') : ?>
		<div class="col-md-12 text-center">
			<h4>Your cart is currently empty.</h4>
			<a href="index.php" class="btn btn-default" style="margin-top:10px;">Return to shop</a>
		</div>
		<?php else : ?>
		<div class="col-md-8">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<th>Product</th><th>Quantity</th><th>Price</th><th>Total</th>
					</thead>
					<tbody>
						<?php
						foreach($items as $item){
							$pID = $item['id'];
							$productQ = $conn->query("SELECT * FROM products WHERE id = '{$pID}'");
							$product = mysqli_fetch_assoc($productQ);
						?>
						<tr>
							<td>
								<div class="pull-left">
									<a href="details.php?id=<?php echo $product['id']; ?>" title="<?php echo $product['name']; ?>">
										<?php $photos = explode(',',$product['image']) ?>
										<img height="40" width="40" src="<?php echo $photos[0]; ?>" alt="<?php echo $product['name']; ?>"/>
									</a>
								</div>
								<div class="pull-left" style="margin-left:20px;">
									<a style="text-decoration:none;color:#333;" href="details.php?id=<?php echo $product['id']; ?>" title="<?php echo $product['name']; ?>">
										<?php echo $product['name']; ?><br>
									</a>
									<small class="text-muted">Size: <?php echo $item['size']; ?></small>
								</div>
								<div style="clear:both"></div>
							</td>
							<td>
								<button class="btn btn-xs btn-default" onclick="update_cart('sub', '<?php echo $product['id']; ?>', '<?php echo $item['size']; ?>');">-</button>
								<?php echo $item['quantity']; ?>
								<button class="btn btn-xs btn-default" onclick="update_cart('add', '<?php echo $product['id']; ?>', '<?php echo $item['size']; ?>');">+</button>
							</td>
							<td><?php echo money($product['price']); ?></td>
							<td><?php echo money($item['quantity'] * $product['price']); ?></td>
						</tr>
						<?php
							$item_count += $item['quantity'];
							$subtotal += ($product['price'] * $item['quantity']);
						}
						$ordertotal = $subtotal;
						?>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-default">
				<div class="panel-heading text-center"><h4>Cart Totals</h4></div>
				<div class="panel-body">
					<table class="table">
						<tbody>
							<tr>
								<td>Total Items</td>
								<td><?php echo $item_count; ?></td>
							</tr>
							<tr>
								<td>Cart Subtotal</td>
								<td><?php echo money($subtotal); ?></td>
							</tr>
							<tr>
								<td>Order Total</td>
								<td><?php echo money($ordertotal); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div>
				<a href="cart.php?checkout=1" class="btn btn-primary btn-lg btn-block">
					Proceed To Checkout
				</a>
			</div>
		</div>

		<?php endif; ?>
	</div>
</div>

<?php } include 'includes/footer.php'; ?>