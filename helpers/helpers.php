<?php
	function display_errors($errors) {
		$display = '<div style="margin-top:-20px;"><ul class="alert text-center" style="list-style:none; margin:0;">';
		foreach ($errors as $error) {
			$display .= '<li class="text-danger text-center" style="display:inline;">'.$error.'</li> ';
		}
		$display .= '</ul></div>';
		return $display;
	}
	
	function sanitize($dirty) {
		return htmlentities($dirty, ENT_QUOTES, "UTF-8");
	}
	
	function money($number) {
		return '&#8369;'.number_format($number,2);
	}
	
	function login_admin($user_id){
		$_SESSION['User'] = $user_id;
		global $conn;
		$date = date("Y-m-d H:i:s");
		$conn->query("UPDATE users SET last_login = '$date' WHERE id = '$user_id'");
		//$_SESSION['success_flash'] = 'You are now logged in.';
		header('Location: index.php');
	}

	function login_editor($user_id){
		$_SESSION['User'] = $user_id;
		global $conn;
		$date = date("Y-m-d H:i:s");
		$conn->query("UPDATE users SET last_login = '$date' WHERE id = '$user_id'");
		//$_SESSION['success_flash'] = 'You are now logged in.';
		header('Location: index.php');
	}

	function login_cust($user_id){
		$_SESSION['User'] = $user_id;
		global $conn;
		$date = date("Y-m-d H:i:s");
		$conn->query("UPDATE users SET last_login = '$date' WHERE id = '$user_id'");
		//$_SESSION['success_flash'] = 'You are now logged in.';
		header('Location: index.php');
	}
	
	function is_logged_in(){
		if(isset($_SESSION['User']) && $_SESSION > 0){
			return true;
		}
		return false;
	}
	
	function login_error_redirect($url = 'login.php'){
		$_SESSION['error_flash'] = 'You do not have permission to access this page.';
		header('Location: '.$url);
	}
	
	function permission_error_redirect($url = 'login.php'){
		$_SESSION['error_flash'] = 'You do not have permission to access this page.';
		header('Location: '.$url);
	}
	
	function has_permission($permission = 'admin'){
		global $user_data;
		$permissions = explode(',', $user_data['permissions']);
		// check if the 'admin' is in the $permissions array
		if(in_array($permission, $permissions, true)){
			return true;
		}
		return false;
	}
	
	function pretty_date($date){
		return date("M d, Y h:i A", strtotime($date));
	}

	function pretty_date2($date){
		return date("M d, Y", strtotime($date));
	}

	function get_category($child_id){
		global $conn;
		$id = sanitize($child_id);
		$sql = "SELECT p.id AS 'pid', p.category AS 'parent', c.id AS 'cid', c.category AS 'child' 
				FROM categories c
				INNER JOIN categories p
				ON c.parent = p.id
				WHERE c.id = '$child_id'";
		$query = $conn->query($sql);
		$category = mysqli_fetch_assoc($query);
		return $category;
	}
?>