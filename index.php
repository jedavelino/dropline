<?php
	require_once 'core/init.php';
	include 'includes/head.php';
	include 'includes/navigation.php';
	$result = $conn->query("SELECT * FROM products WHERE featured = 1");
?>

<div class="container">
	<h3 class="text-center">Featured Product</h3>
	<hr>
	<?php while($product = mysqli_fetch_assoc($result)) : ?>
	<div class="col-md-4 col-sm-6 col-xs-6">
		<div class="thumbnail">
			<?php $photos = explode(',',$product['image']); ?>
			<a href="details.php?id=<?php echo $product['id']; ?>">
				<div><!-- <div style="padding:50px;"> -->
					<img class="img-responsive" src="<?php echo $photos[0]; ?>" alt="<?php echo $product['name']; ?>">
				</div>
			</a>
			<div style="width:10%;height:1px;margin:0 auto;background:#333;"></div>
			<div class="caption cap">
				<p class="text-center product-name"><strong><?php echo $product['name']; ?></strong></p>
				<p class="text-center price"><?php echo money($product['price']); ?></p>
				<button class="btn btn-default quick-view-btn btn-block" onclick="details_modal(<?php echo $product['id']; ?>);" type="button">
					Quick View
				</button>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
</div>
	
<?php include 'includes/footer.php'; ?>